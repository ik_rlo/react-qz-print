import React from 'react';
interface Props {
    name: string;
    host?: string;
}
declare class Print extends React.Component<Props> {
    config: unknown;
    constructor(props: Props);
    componentDidMount(): void;
    connect: (host: string, name: string) => Promise<void>;
    print: () => void;
    render(): unknown;
}
export default Print;
