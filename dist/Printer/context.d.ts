/// <reference types="react" />
export declare type ConnectionInfo = {
    socket: string;
    host: string;
    port: number;
};
export declare type PrinterContext = {
    print: (data: string) => Promise<undefined>;
    listPrinters: (query?: string) => Promise<Object>;
    getPrinter: () => Object;
    setPrinter: (name: string, options?: Object) => Object;
    setDefaultPrinter: (options?: Object) => Promise<Object>;
    isActive: () => boolean;
    getConnectionInfo: () => Promise<ConnectionInfo>;
    connect: () => Promise<Object>;
    disconnect: () => Promise<null>;
    isConnected: boolean;
    encoder: Object;
};
declare const printerContext: import("react").Context<PrinterContext>;
export default printerContext;
