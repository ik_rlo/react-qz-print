import React from 'react';
declare type ConnectionOptions = {
    keepAlive?: number;
    retries?: number;
};
declare type Props = {
    host?: string;
    name?: string;
    certEndpoint: string;
    signEndpoint: string;
    options?: ConnectionOptions;
};
declare type State = {
    isConnected: boolean;
};
declare class PrinterProvider extends React.Component<Props, State> {
    config: Object;
    options: ConnectionOptions;
    encoder: Object;
    constructor(props: Props);
    componentDidMount(): Promise<void>;
    onCloseConnection: () => void;
    connect: () => Promise<Object>;
    disconnect: () => Promise<any>;
    isActive: () => any;
    getConnectionInfo: () => any;
    print: (data: string) => Promise<any>;
    getPrinter: () => Object;
    listPrinters: (query: string) => Promise<any>;
    setPrinter: (name: string, options?: Object) => Object;
    setDefaultPrinter: (options?: Object) => Promise<Object>;
    render(): JSX.Element;
}
export default PrinterProvider;
