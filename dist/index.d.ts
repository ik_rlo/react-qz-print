/// <reference types="react" />
import Provider from './provider';
import withPrinter from './withPrinter';
import { PrinterContext } from './context';
import Encoder from './lib/escpos';
export { Provider, withPrinter, PrinterContext, Encoder };
declare const _default: {
    Provider: typeof Provider;
    withPrinter: (Component: import("react").ElementType<any>) => import("react").FunctionComponent<{}>;
    Encoder: any;
};
export default _default;
