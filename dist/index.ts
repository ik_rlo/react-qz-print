'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function createCommonjsModule(fn, basedir, module) {
	return module = {
	  path: basedir,
	  exports: {},
	  require: function (path, base) {
      return commonjsRequire(path, (base === undefined || base === null) ? module.path : base);
    }
	}, fn(module, module.exports), module.exports;
}

function commonjsRequire () {
	throw new Error('Dynamic requires are not currently supported by @rollup/plugin-commonjs');
}

var qzTray = createCommonjsModule(function (module, exports) {

/**
 * @version 2.1.1+1
 * @overview QZ Tray Connector
 * <p/>
 * Connects a web client to the QZ Tray software.
 * Enables printing and device communication from javascript.
 */
var qz = (function() {

///// POLYFILLS /////
    if (!Array.isArray) {
        Array.isArray = function(arg) {
            return Object.prototype.toString.call(arg) === '[object Array]';
        };
    }

    if (!Number.isInteger) {
        Number.isInteger = function(value) {
            return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
        };
    }


    // from SHA implementation
    if (typeof String.prototype.utf8Encode == 'undefined') {
        String.prototype.utf8Encode = function() { return unescape(encodeURIComponent(this)); };
    }
    if (typeof String.prototype.utf8Decode == 'undefined') {
        String.prototype.utf8Decode = function() {
            try { return decodeURIComponent(escape(this)); }
            catch(e) { return this; } // invalid UTF-8? return as-is
        };
    }

///// PRIVATE METHODS /////

    var _qz = {
        VERSION: "2.1.1+1",                              //must match @version above
        DEBUG: false,

        log: {
            /** Debugging messages */
            trace: function() { if (_qz.DEBUG) { console.log.apply(console, arguments); } },
            /** General messages */
            info: function() { console.info.apply(console, arguments); },
            /** General warnings */
            warn: function() { console.warn.apply(console, arguments); },
            /** Debugging errors */
            allay: function() { if (_qz.DEBUG) { console.warn.apply(console, arguments); } },
            /** General errors */
            error: function() { console.error.apply(console, arguments); }
        },


        //stream types
        streams: {
            serial: 'SERIAL', usb: 'USB', hid: 'HID', printer: 'PRINTER', file: 'FILE'
        },


        websocket: {
            /** The actual websocket object managing the connection. */
            connection: null,

            /** Default parameters used on new connections. Override values using options parameter on {@link qz.websocket.connect}. */
            connectConfig: {
                host: ["localhost", "localhost.qz.io"], //hosts QZ Tray can be running on
                hostIndex: 0,                           //internal var - index on host array
                usingSecure: true,                      //boolean use of secure protocol
                protocol: {
                    secure: "wss://",                   //secure websocket
                    insecure: "ws://"                   //insecure websocket
                },
                port: {
                    secure: [8181, 8282, 8383, 8484],   //list of secure ports QZ Tray could be listening on
                    insecure: [8182, 8283, 8384, 8485], //list of insecure ports QZ Tray could be listening on
                    portIndex: 0                        //internal var - index on active port array
                },
                keepAlive: 60,                          //time between pings to keep connection alive, in seconds
                retries: 0,                             //number of times to reconnect before failing
                delay: 0                                //seconds before firing a connection
            },

            setup: {
                /** Loop through possible ports to open connection, sets web socket calls that will settle the promise. */
                findConnection: function(config, resolve, reject) {
                    //force flag if missing ports
                    if (!config.port.secure.length) {
                        if (!config.port.insecure.length) {
                            reject(new Error("No ports have been specified to connect over"));
                            return;
                        } else if (config.usingSecure) {
                            _qz.log.error("No secure ports specified - forcing insecure connection");
                            config.usingSecure = false;
                        }
                    } else if (!config.port.insecure.length && !config.usingSecure) {
                        _qz.log.trace("No insecure ports specified - forcing secure connection");
                        config.usingSecure = true;
                    }

                    var deeper = function() {
                        config.port.portIndex++;

                        if ((config.usingSecure && config.port.portIndex >= config.port.secure.length)
                            || (!config.usingSecure && config.port.portIndex >= config.port.insecure.length)) {
                            if (config.hostIndex >= config.host.length - 1) {
                                //give up, all hope is lost
                                reject(new Error("Unable to establish connection with QZ"));
                                return;
                            } else {
                                config.hostIndex++;
                                config.port.portIndex = 0;
                            }
                        }

                        // recursive call until connection established or all ports are exhausted
                        _qz.websocket.setup.findConnection(config, resolve, reject);
                    };

                    var address;
                    if (config.usingSecure) {
                        address = config.protocol.secure + config.host[config.hostIndex] + ":" + config.port.secure[config.port.portIndex];
                    } else {
                        address = config.protocol.insecure + config.host[config.hostIndex] + ":" + config.port.insecure[config.port.portIndex];
                    }

                    try {
                        _qz.log.trace("Attempting connection", address);
                        _qz.websocket.connection = new _qz.tools.ws(address);
                    }
                    catch(err) {
                        _qz.log.error(err);
                        deeper();
                        return;
                    }

                    if (_qz.websocket.connection != null) {
                        _qz.websocket.connection.established = false;

                        //called on successful connection to qz, begins setup of websocket calls and resolves connect promise after certificate is sent
                        _qz.websocket.connection.onopen = function(evt) {
                            if (!_qz.websocket.connection.established) {
                                _qz.log.trace(evt);
                                _qz.log.info("Established connection with QZ Tray on " + address);

                                _qz.websocket.setup.openConnection({ resolve: resolve, reject: reject });

                                if (config.keepAlive > 0) {
                                    var interval = setInterval(function() {
                                        if (!_qz.tools.isActive()) {
                                            clearInterval(interval);
                                            return;
                                        }

                                        _qz.websocket.connection.send("ping");
                                    }, config.keepAlive * 1000);
                                }
                            }
                        };

                        //called during websocket close during setup
                        _qz.websocket.connection.onclose = function() {
                            // Safari compatibility fix to raise error event
                            if (_qz.websocket.connection && typeof navigator !== 'undefined' && navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
                                _qz.websocket.connection.onerror();
                            }
                        };

                        //called for errors during setup (such as invalid ports), reject connect promise only if all ports have been tried
                        _qz.websocket.connection.onerror = function(evt) {
                            _qz.log.trace(evt);

                            _qz.websocket.connection = null;

                            deeper();
                        };
                    } else {
                        reject(new Error("Unable to create a websocket connection"));
                    }
                },

                /** Finish setting calls on successful connection, sets web socket calls that won't settle the promise. */
                openConnection: function(openPromise) {
                    _qz.websocket.connection.established = true;

                    //called when an open connection is closed
                    _qz.websocket.connection.onclose = function(evt) {
                        _qz.log.trace(evt);
                        _qz.log.info("Closed connection with QZ Tray");

                        //if this is set, then an explicit close call was made
                        if (_qz.websocket.connection.promise != undefined) {
                            _qz.websocket.connection.promise.resolve();
                        }

                        _qz.websocket.callClose(evt);
                        _qz.websocket.connection = null;

                        for(var uid in _qz.websocket.pendingCalls) {
                            if (_qz.websocket.pendingCalls.hasOwnProperty(uid)) {
                                _qz.websocket.pendingCalls[uid].reject(new Error("Connection closed before response received"));
                            }
                        }
                    };

                    //called for any errors with an open connection
                    _qz.websocket.connection.onerror = function(evt) {
                        _qz.websocket.callError(evt);
                    };

                    //send JSON objects to qz
                    _qz.websocket.connection.sendData = function(obj) {
                        _qz.log.trace("Preparing object for websocket", obj);

                        if (obj.timestamp == undefined) {
                            obj.timestamp = Date.now();
                            if (typeof obj.timestamp !== 'number') {
                                obj.timestamp = new Date().getTime();
                            }
                        }
                        if (obj.promise != undefined) {
                            obj.uid = _qz.websocket.setup.newUID();
                            _qz.websocket.pendingCalls[obj.uid] = obj.promise;
                        }

                        //ensure we know how this was signed
                        obj.signAlgorithm = _qz.security.signAlgorithm;

                        // track requesting monitor
                        obj.position = {
                            x: typeof screen !== 'undefined' ? ((screen.availWidth || screen.width) / 2) + (screen.left || screen.availLeft) : 0,
                            y: typeof screen !== 'undefined' ? ((screen.availHeight || screen.height) / 2) + (screen.top || screen.availTop) : 0
                        };

                        try {
                            if (obj.call != undefined && obj.signature == undefined) {
                                var signObj = {
                                    call: obj.call,
                                    params: obj.params,
                                    timestamp: obj.timestamp
                                };

                                //make a hashing promise if not already one
                                var hashing = _qz.tools.hash(_qz.tools.stringify(signObj));
                                if (!hashing.then) {
                                    hashing = _qz.tools.promise(function(resolve) {
                                        resolve(hashing);
                                    });
                                }

                                hashing.then(function(hashed) {
                                    return _qz.security.callSign(hashed);
                                }).then(function(signature) {
                                    _qz.log.trace("Signature for call", signature);
                                    obj.signature = signature;

                                    _qz.signContent = undefined;
                                    _qz.websocket.connection.send(_qz.tools.stringify(obj));
                                });
                            } else {
                                _qz.log.trace("Signature for call", obj.signature);

                                //called for pre-signed content and (unsigned) setup calls
                                _qz.websocket.connection.send(_qz.tools.stringify(obj));
                            }
                        }
                        catch(err) {
                            _qz.log.error(err);

                            if (obj.promise != undefined) {
                                obj.promise.reject(err);
                                delete _qz.websocket.pendingCalls[obj.uid];
                            }
                        }
                    };

                    //receive message from qz
                    _qz.websocket.connection.onmessage = function(evt) {
                        var returned = JSON.parse(evt.data);

                        if (returned.uid == null) {
                            if (returned.type == null) {
                                //incorrect response format, likely connected to incompatible qz version
                                _qz.websocket.connection.close(4003, "Connected to incompatible QZ Tray version");

                            } else {
                                //streams (callbacks only, no promises)
                                switch(returned.type) {
                                    case _qz.streams.serial:
                                        if (!returned.event) {
                                            returned.event = JSON.stringify({ portName: returned.key, output: returned.data });
                                        }

                                        _qz.serial.callSerial(JSON.parse(returned.event));
                                        break;
                                    case _qz.streams.usb:
                                        if (!returned.event) {
                                            returned.event = JSON.stringify({ vendorId: returned.key[0], productId: returned.key[1], output: returned.data });
                                        }

                                        _qz.usb.callUsb(JSON.parse(returned.event));
                                        break;
                                    case _qz.streams.hid:
                                        _qz.hid.callHid(JSON.parse(returned.event));
                                        break;
                                    case _qz.streams.printer:
                                        _qz.printers.callPrinter(JSON.parse(returned.event));
                                        break;
                                    case _qz.streams.file:
                                        _qz.file.callFile(JSON.parse(returned.event));
                                        break;
                                    default:
                                        _qz.log.allay("Cannot determine stream type for callback", returned);
                                        break;
                                }
                            }

                            return;
                        }

                        _qz.log.trace("Received response from websocket", returned);

                        var promise = _qz.websocket.pendingCalls[returned.uid];
                        if (promise == undefined) {
                            _qz.log.allay('No promise found for returned response');
                        } else {
                            if (returned.error != undefined) {
                                promise.reject(new Error(returned.error));
                            } else {
                                promise.resolve(returned.result);
                            }
                        }

                        delete _qz.websocket.pendingCalls[returned.uid];
                    };


                    //send up the certificate before making any calls
                    //also gives the user a chance to deny the connection
                    function sendCert(cert) {
                        if (cert === undefined) { cert = null; }

                        //websocket setup, query what version is connected
                        qz.api.getVersion().then(function(version) {
                            _qz.websocket.connection.version = version;
                            _qz.websocket.connection.semver = version.toLowerCase().replace(/-rc\./g, "-rc").split(/[\\+\\.-]/g);
                            for(var i = 0; i < _qz.websocket.connection.semver.length; i++) {
                                try {
                                    if (i == 3 && _qz.websocket.connection.semver[i].toLowerCase().indexOf("rc") == 0) {
                                        // Handle "rc1" pre-release by negating build info
                                        _qz.websocket.connection.semver[i] = -(_qz.websocket.connection.semver[i].replace(/\D/g, ""));
                                        continue;
                                    }
                                    _qz.websocket.connection.semver[i] = parseInt(_qz.websocket.connection.semver[i]);
                                }
                                catch(ignore) {}

                                if (_qz.websocket.connection.semver.length < 4) {
                                    _qz.websocket.connection.semver[3] = 0;
                                }
                            }

                            //algorithm can be declared before a connection, check for incompatibilities now that we have one
                            _qz.compatible.algorithm(true);
                        }).then(function() {
                            _qz.websocket.connection.sendData({ certificate: cert, promise: openPromise });
                        });
                    }

                    _qz.security.callCert().then(sendCert).catch(sendCert);
                },

                /** Generate unique ID used to map a response to a call. */
                newUID: function() {
                    var len = 6;
                    return (new Array(len + 1).join("0") + (Math.random() * Math.pow(36, len) << 0).toString(36)).slice(-len)
                }
            },

            dataPromise: function(callName, params, signature, signingTimestamp) {
                return _qz.tools.promise(function(resolve, reject) {
                    var msg = {
                        call: callName,
                        promise: { resolve: resolve, reject: reject },
                        params: params,
                        signature: signature,
                        timestamp: signingTimestamp
                    };

                    _qz.websocket.connection.sendData(msg);
                });
            },

            /** Library of promises awaiting a response, uid -> promise */
            pendingCalls: {},

            /** List of functions to call on error from the websocket. */
            errorCallbacks: [],
            /** Calls all functions registered to listen for errors. */
            callError: function(evt) {
                if (Array.isArray(_qz.websocket.errorCallbacks)) {
                    for(var i = 0; i < _qz.websocket.errorCallbacks.length; i++) {
                        _qz.websocket.errorCallbacks[i](evt);
                    }
                } else {
                    _qz.websocket.errorCallbacks(evt);
                }
            },

            /** List of function to call on closing from the websocket. */
            closedCallbacks: [],
            /** Calls all functions registered to listen for closing. */
            callClose: function(evt) {
                if (Array.isArray(_qz.websocket.closedCallbacks)) {
                    for(var i = 0; i < _qz.websocket.closedCallbacks.length; i++) {
                        _qz.websocket.closedCallbacks[i](evt);
                    }
                } else {
                    _qz.websocket.closedCallbacks(evt);
                }
            }
        },


        printing: {
            /** Default options used for new printer configs. Can be overridden using {@link qz.configs.setDefaults}. */
            defaultConfig: {
                //value purposes are explained in the qz.configs.setDefaults docs

                bounds: null,
                colorType: 'color',
                copies: 1,
                density: 0,
                duplex: false,
                fallbackDensity: null,
                interpolation: 'bicubic',
                jobName: null,
                legacy: false,
                margins: 0,
                orientation: null,
                paperThickness: null,
                printerTray: null,
                rasterize: false,
                rotation: 0,
                scaleContent: true,
                size: null,
                units: 'in',

                altPrinting: false,
                encoding: null,
                endOfDoc: null,
                perSpool: 1
            }
        },


        serial: {
            /** List of functions called when receiving data from serial connection. */
            serialCallbacks: [],
            /** Calls all functions registered to listen for serial events. */
            callSerial: function(streamEvent) {
                if (Array.isArray(_qz.serial.serialCallbacks)) {
                    for(var i = 0; i < _qz.serial.serialCallbacks.length; i++) {
                        _qz.serial.serialCallbacks[i](streamEvent);
                    }
                } else {
                    _qz.serial.serialCallbacks(streamEvent);
                }
            }
        },


        usb: {
            /** List of functions called when receiving data from usb connection. */
            usbCallbacks: [],
            /** Calls all functions registered to listen for usb events. */
            callUsb: function(streamEvent) {
                if (Array.isArray(_qz.usb.usbCallbacks)) {
                    for(var i = 0; i < _qz.usb.usbCallbacks.length; i++) {
                        _qz.usb.usbCallbacks[i](streamEvent);
                    }
                } else {
                    _qz.usb.usbCallbacks(streamEvent);
                }
            }
        },


        hid: {
            /** List of functions called when receiving data from hid connection. */
            hidCallbacks: [],
            /** Calls all functions registered to listen for hid events. */
            callHid: function(streamEvent) {
                if (Array.isArray(_qz.hid.hidCallbacks)) {
                    for(var i = 0; i < _qz.hid.hidCallbacks.length; i++) {
                        _qz.hid.hidCallbacks[i](streamEvent);
                    }
                } else {
                    _qz.hid.hidCallbacks(streamEvent);
                }
            }
        },


        printers: {
            /** List of functions called when receiving data from printer connection. */
            printerCallbacks: [],
            /** Calls all functions registered to listen for printer events. */
            callPrinter: function(streamEvent) {
                if (Array.isArray(_qz.printers.printerCallbacks)) {
                    for(var i = 0; i < _qz.printers.printerCallbacks.length; i++) {
                        _qz.printers.printerCallbacks[i](streamEvent);
                    }
                } else {
                    _qz.printers.printerCallbacks(streamEvent);
                }
            }
        },


        file: {
            /** List of functions called when receiving info regarding file changes. */
            fileCallbacks: [],
            /** Calls all functions registered to listen for file events. */
            callFile: function(streamEvent) {
                if (Array.isArray(_qz.file.fileCallbacks)) {
                    for(var i = 0; i < _qz.file.fileCallbacks.length; i++) {
                        _qz.file.fileCallbacks[i](streamEvent);
                    }
                } else {
                    _qz.file.fileCallbacks(streamEvent);
                }
            }
        },


        security: {
            /** Function used to resolve promise when acquiring site's public certificate. */
            certHandler: function(resolve, reject) { reject(); },
            /** Called to create new promise (using {@link _qz.security.certHandler}) for certificate retrieval. */
            callCert: function() {
                if (typeof _qz.security.certHandler.then === 'function' || _qz.security.certHandler.constructor.name === "AsyncFunction") {
                    //already a promise
                    return _qz.security.certHandler();
                } else {
                    //turn into a promise
                    return _qz.tools.promise(_qz.security.certHandler);
                }
            },

            /** Function used to create promise resolver when requiring signed calls. */
            signatureFactory: function() { return function(resolve) { resolve(); } },
            /** Called to create new promise (using {@link _qz.security.signatureFactory}) for signed calls. */
            callSign: function(toSign) {
                if (typeof _qz.security.signatureFactory.then === 'function' || _qz.security.signatureFactory.constructor.name === "AsyncFunction") {
                    //already a promise
                    return _qz.security.signatureFactory(toSign);
                } else {
                    //turn into a promise
                    return _qz.tools.promise(_qz.security.signatureFactory(toSign));
                }
            },

            /** Signing algorithm used on signatures */
            signAlgorithm: "SHA1"
        },


        tools: {
            /** Create a new promise */
            promise: function(resolver) {
                //prefer global object for historical purposes
                if (typeof RSVP !== 'undefined') {
                    return new RSVP.Promise(resolver);
                } else if (typeof Promise !== 'undefined') {
                    return new Promise(resolver);
                } else {
                    _qz.log.error("Promise/A+ support is required.  See qz.api.setPromiseType(...)");
                }
            },

            stringify: function(object) {
                //old versions of prototype affect stringify
                var pjson = Array.prototype.toJSON;
                delete Array.prototype.toJSON;

                var result = JSON.stringify(object);

                if (pjson) {
                    Array.prototype.toJSON = pjson;
                }

                return result;
            },

            hash: function(data) {
                //prefer global object for historical purposes
                if (typeof Sha256 !== 'undefined') {
                    return Sha256.hash(data);
                } else {
                    return _qz.SHA.hash(data);
                }
            },

            ws: typeof WebSocket !== 'undefined' ? WebSocket : null,

            absolute: function(loc) {
                if (typeof window !== 'undefined' && typeof document.createElement === 'function') {
                    var a = document.createElement("a");
                    a.href = loc;
                    return a.href;
                }
                return loc;
            },

            relative: function(data) {
                for(var i = 0; i < data.length; i++) {
                    if (data[i].constructor === Object) {
                        var absolute = false;

                        if (data[i].data.search(/data:image\/\w+;base64,/) === 0) {
                            //upgrade from old base64 behavior
                            data[i].flavor = "base64";
                            data[i].data = data[i].data.replace(/^data:image\/\w+;base64,/, "");
                        } else if (data[i].flavor) {
                            //if flavor is known, we can directly check for absolute flavor types
                            if (["FILE", "XML"].indexOf(data[i].flavor.toUpperCase()) > -1) {
                                absolute = true;
                            }
                        } else if (data[i].format && ["HTML", "IMAGE", "PDF", "FILE", "XML"].indexOf(data[i].format.toUpperCase()) > -1) {
                            //if flavor is not known, all valid pixel formats default to file flavor
                            //previous v2.0 data also used format as what is now flavor, so we check for those values here too
                            absolute = true;
                        } else if (data[i].type && ((["PIXEL", "IMAGE", "PDF"].indexOf(data[i].type.toUpperCase()) > -1 && !data[i].format)
                            || (["HTML", "PDF"].indexOf(data[i].type.toUpperCase()) > -1 && (!data[i].format || data[i].format.toUpperCase() === "FILE")))) {
                            //if all we know is pixel type, then it is image's file flavor
                            //previous v2.0 data also used type as what is now format, so we check for those value here too
                            absolute = true;
                        }

                        if (absolute) {
                            //change relative links to absolute
                            data[i].data = _qz.tools.absolute(data[i].data);
                        }
                        if (data[i].options && typeof data[i].options.overlay === 'string') {
                            data[i].options.overlay = _qz.tools.absolute(data[i].options.overlay);
                        }
                    }
                }
            },

            /** Performs deep copy to target from remaining params */
            extend: function(target) {
                //special case when reassigning properties as objects in a deep copy
                if (typeof target !== 'object') {
                    target = {};
                }

                for(var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    if (!source) { continue; }

                    for(var key in source) {
                        if (source.hasOwnProperty(key)) {
                            if (target === source[key]) { continue; }

                            if (source[key] && source[key].constructor && source[key].constructor === Object) {
                                var clone;
                                if (Array.isArray(source[key])) {
                                    clone = target[key] || [];
                                } else {
                                    clone = target[key] || {};
                                }

                                target[key] = _qz.tools.extend(clone, source[key]);
                            } else if (source[key] !== undefined) {
                                target[key] = source[key];
                            }
                        }
                    }
                }

                return target;
            },

            versionCompare: function(major, minor, patch, build) {
                if (_qz.tools.assertActive()) {
                    var semver = _qz.websocket.connection.semver;
                    if (semver[0] != major) {
                        return semver[0] - major;
                    }
                    if (minor != undefined && semver[1] != minor) {
                        return semver[1] - minor;
                    }
                    if (patch != undefined && semver[2] != patch) {
                        return semver[2] - patch;
                    }
                    if (build != undefined && semver.length > 3 && semver[3] != build) {
                        return Number.isInteger(semver[3]) && Number.isInteger(build) ? semver[3] - build : semver[3].toString().localeCompare(build.toString());
                    }
                    return 0;
                }
            },

            isVersion: function(major, minor, patch, build) {
                return _qz.tools.versionCompare(major, minor, patch, build) == 0;
            },

            isActive: function() {
                return _qz.websocket.connection != null && _qz.websocket.connection.established;
            },

            assertActive: function() {
                if (_qz.tools.isActive()) {
                    return true;
                }
                // Promise won't reject on throw; yet better than 'undefined'
                throw new Error("A connection to QZ has not been established yet");
            }
        },

        compatible: {
            /** Converts message format to a previous version's */
            data: function(printData) {
                if (_qz.tools.isVersion(2, 0)) {
                    /*
                    2.0.x conversion
                    -----
                    type=pixel -> use format as 2.0 type (unless 'command' format, which forces 2.0 'raw' type)
                    type=raw -> 2.0 type has to be 'raw'
                                if format is 'image' -> force 2.0 'image' format, ignore everything else (unsupported in 2.0)

                     flavor translates straight to 2.0 format (unless forced to 'raw'/'image')
                     */
                    _qz.log.trace("Converting print data to v2.0 for " + _qz.websocket.connection.version);
                    for(var i = 0; i < printData.length; i++) {
                        if (printData[i].constructor === Object) {
                            if (printData[i].type && printData[i].type.toUpperCase() === "RAW" && printData[i].format && printData[i].format.toUpperCase() === "IMAGE") {
                                if (printData[i].flavor && printData[i].flavor.toUpperCase() === "BASE64") {
                                    //special case for raw base64 images
                                    printData[i].data = "data:image/compat;base64," + printData[i].data;
                                }
                                printData[i].flavor = "IMAGE"; //forces 'image' format when shifting for conversion
                            }
                            if ((printData[i].type && printData[i].type.toUpperCase() === "RAW") || (printData[i].format && printData[i].format.toUpperCase() === "COMMAND")) {
                                printData[i].format = "RAW"; //forces 'raw' type when shifting for conversion
                            }

                            printData[i].type = printData[i].format;
                            printData[i].format = printData[i].flavor;
                            delete printData[i].flavor;
                        }
                    }
                }
            },

            /* Converts config defaults to match previous version */
            config: function(config, dirty) {
                if (_qz.tools.isVersion(2, 0)) {
                    if (!dirty.rasterize) {
                        config.rasterize = true;
                    }
                }

                return config;
            },

            /** Compat wrapper with previous version **/
            networking: function(hostname, port, signature, signingTimestamp, mappingCallback) {
                // Use 2.0
                if (_qz.tools.isVersion(2, 0)) {
                    return _qz.tools.promise(function(resolve, reject) {
                        _qz.websocket.dataPromise('websocket.getNetworkInfo', {
                            hostname: hostname,
                            port: port
                        }, signature, signingTimestamp).then(function(data) {
                            if (typeof mappingCallback !== 'undefined') {
                                resolve(mappingCallback(data));
                            } else {
                                resolve(data);
                            }
                        }, reject);
                    });
                }
                // Wrap 2.1
                return _qz.tools.promise(function(resolve, reject) {
                    _qz.websocket.dataPromise('networking.device', {
                        hostname: hostname,
                        port: port
                    }, signature, signingTimestamp).then(function(data) {
                        resolve({ ipAddress: data.ip, macAddress: data.mac });
                    }, reject);
                });
            },

            /** Check if QZ version supports chosen algorithm */
            algorithm: function(quiet) {
                //if not connected yet we will assume compatibility exists for the time being
                if (_qz.tools.isActive()) {
                    if (_qz.tools.isVersion(2, 0)) {
                        if (!quiet) {
                            _qz.log.warn("Connected to an older version of QZ, alternate signature algorithms are not supported");
                        }
                        return false;
                    }
                }

                return true;
            }
        },

        /**
         * Adapted from Chris Veness's code under MIT Licence (C) 2002
         * see http://www.movable-type.co.uk/scripts/sha256.html
         */
        SHA: {
            //@formatter:off - keep this block compact
            hash: function(msg) {
                // add trailing '1' bit (+ 0's padding) to string [§5.1.1]
                msg = msg.utf8Encode() + String.fromCharCode(0x80);

                // constants [§4.2.2]
                var K = [
                    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
                    0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
                    0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
                    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
                    0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
                    0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
                    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
                    0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
                ];
                // initial hash value [§5.3.1]
                var H = [ 0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19 ];

                // convert string msg into 512-bit/16-integer blocks arrays of ints [§5.2.1]
                var l = msg.length / 4 + 2; // length (in 32-bit integers) of msg + ‘1’ + appended length
                var N = Math.ceil(l / 16);  // number of 16-integer-blocks required to hold 'l' ints
                var M = new Array(N);

                for(var i = 0; i < N; i++) {
                    M[i] = new Array(16);
                    for(var j = 0; j < 16; j++) {  // encode 4 chars per integer, big-endian encoding
                        M[i][j] = (msg.charCodeAt(i * 64 + j * 4) << 24) | (msg.charCodeAt(i * 64 + j * 4 + 1) << 16) |
                            (msg.charCodeAt(i * 64 + j * 4 + 2) << 8) | (msg.charCodeAt(i * 64 + j * 4 + 3));
                    } // note running off the end of msg is ok 'cos bitwise ops on NaN return 0
                }
                // add length (in bits) into final pair of 32-bit integers (big-endian) [§5.1.1]
                // note: most significant word would be (len-1)*8 >>> 32, but since JS converts
                // bitwise-op args to 32 bits, we need to simulate this by arithmetic operators
                M[N-1][14] = ((msg.length - 1) * 8) / Math.pow(2, 32);
                M[N-1][14] = Math.floor(M[N-1][14]);
                M[N-1][15] = ((msg.length - 1) * 8) & 0xffffffff;

                // HASH COMPUTATION [§6.1.2]
                var W = new Array(64); var a, b, c, d, e, f, g, h;
                for(var i = 0; i < N; i++) {
                    // 1 - prepare message schedule 'W'
                    for(var t = 0; t < 16; t++) { W[t] = M[i][t]; }
                    for(var t = 16; t < 64; t++) { W[t] = (_qz.SHA._dev1(W[t-2]) + W[t-7] + _qz.SHA._dev0(W[t-15]) + W[t-16]) & 0xffffffff; }
                    // 2 - initialise working variables a, b, c, d, e, f, g, h with previous hash value
                    a = H[0]; b = H[1]; c = H[2]; d = H[3]; e = H[4]; f = H[5]; g = H[6]; h = H[7];
                    // 3 - main loop (note 'addition modulo 2^32')
                    for(var t = 0; t < 64; t++) {
                        var T1 = h + _qz.SHA._sig1(e) + _qz.SHA._ch(e, f, g) + K[t] + W[t];
                        var T2 = _qz.SHA._sig0(a) + _qz.SHA._maj(a, b, c);
                        h = g; g = f; f = e; e = (d + T1) & 0xffffffff;
                        d = c; c = b; b = a; a = (T1 + T2) & 0xffffffff;
                    }
                    // 4 - compute the new intermediate hash value (note 'addition modulo 2^32')
                    H[0] = (H[0]+a) & 0xffffffff; H[1] = (H[1]+b) & 0xffffffff; H[2] = (H[2]+c) & 0xffffffff; H[3] = (H[3]+d) & 0xffffffff;
                    H[4] = (H[4]+e) & 0xffffffff; H[5] = (H[5]+f) & 0xffffffff; H[6] = (H[6]+g) & 0xffffffff; H[7] = (H[7]+h) & 0xffffffff;
                }

                return _qz.SHA._hexStr(H[0]) + _qz.SHA._hexStr(H[1]) + _qz.SHA._hexStr(H[2]) + _qz.SHA._hexStr(H[3]) +
                    _qz.SHA._hexStr(H[4]) + _qz.SHA._hexStr(H[5]) + _qz.SHA._hexStr(H[6]) + _qz.SHA._hexStr(H[7]);
            },

            // Rotates right (circular right shift) value x by n positions
            _rotr: function(n, x) { return (x >>> n) | (x << (32 - n)); },
            // logical functions
            _sig0: function(x) { return _qz.SHA._rotr(2, x) ^ _qz.SHA._rotr(13, x) ^ _qz.SHA._rotr(22, x); },
            _sig1: function(x) { return _qz.SHA._rotr(6, x) ^ _qz.SHA._rotr(11, x) ^ _qz.SHA._rotr(25, x); },
            _dev0: function(x) { return _qz.SHA._rotr(7, x) ^ _qz.SHA._rotr(18, x) ^ (x >>> 3); },
            _dev1: function(x) { return _qz.SHA._rotr(17, x) ^ _qz.SHA._rotr(19, x) ^ (x >>> 10); },
            _ch: function(x, y, z) { return (x & y) ^ (~x & z); },
            _maj: function(x, y, z) { return (x & y) ^ (x & z) ^ (y & z); },
            // note can't use toString(16) as it is implementation-dependant, and in IE returns signed numbers when used on full words
            _hexStr: function(n) { var s = "", v; for(var i = 7; i >= 0; i--) { v = (n >>> (i * 4)) & 0xf; s += v.toString(16); } return s; },
            //@formatter:on
        },
    };


///// CONFIG CLASS ////

    /** Object to handle configured printer options. */
    function Config(printer, opts) {

        this.config = _qz.tools.extend({}, _qz.printing.defaultConfig); //create a copy of the default options
        this._dirtyOpts = {}; //track which config options have changed from the defaults

        /**
         * Set the printer assigned to this config.
         * @param {string|Object} newPrinter Name of printer. Use object type to specify printing to file or host.
         *  @param {string} [newPrinter.name] Name of printer to send printing.
         *  @param {string} [newPrinter.file] Name of file to send printing.
         *  @param {string} [newPrinter.host] IP address or host name to send printing.
         *  @param {string} [newPrinter.port] Port used by &lt;printer.host>.
         */
        this.setPrinter = function(newPrinter) {
            if (typeof newPrinter === 'string') {
                newPrinter = { name: newPrinter };
            }

            this.printer = newPrinter;
        };

        /**
         *  @returns {Object} The printer currently assigned to this config.
         */
        this.getPrinter = function() {
            return this.printer;
        };

        /**
         * Alter any of the printer options currently applied to this config.
         * @param newOpts {Object} The options to change. See <code>qz.configs.setDefaults</code> docs for available values.
         *
         * @see qz.configs.setDefaults
         */
        this.reconfigure = function(newOpts) {
            for(var key in newOpts) {
                if (newOpts[key] !== undefined) {
                    this._dirtyOpts[key] = true;
                }
            }

            _qz.tools.extend(this.config, newOpts);
        };

        /**
         * @returns {Object} The currently applied options on this config.
         */
        this.getOptions = function() {
            return _qz.compatible.config(this.config, this._dirtyOpts);
        };

        // init calls for new config object
        this.setPrinter(printer);
        this.reconfigure(opts);
    }

    /**
     * Shortcut method for calling <code>qz.print</code> with a particular config.
     * @param {Array<Object|string>} data Array of data being sent to the printer. See <code>qz.print</code> docs for available values.
     * @param {boolean} [signature] Pre-signed signature of JSON string containing <code>call</code>, <code>params</code>, and <code>timestamp</code>.
     * @param {number} [signingTimestamp] Required with <code>signature</code>. Timestamp used with pre-signed content.
     *
     * @example
     * qz.print(myConfig, ...); // OR
     * myConfig.print(...);
     *
     * @see qz.print
     */
    Config.prototype.print = function(data, signature, signingTimestamp) {
        qz.print(this, data, signature, signingTimestamp);
    };


///// PUBLIC METHODS /////

    /** @namespace qz */
    var qz = {

        /**
         * Calls related specifically to the web socket connection.
         * @namespace qz.websocket
         */
        websocket: {
            /**
             * Check connection status. Active connection is necessary for other calls to run.
             *
             * @returns {boolean} If there is an active connection with QZ Tray.
             *
             * @see connect
             *
             * @memberof  qz.websocket
             */
            isActive: function() {
                return _qz.tools.isActive();
            },

            /**
             * Call to setup connection with QZ Tray on user's system.
             *
             * @param {Object} [options] Configuration options for the web socket connection.
             *  @param {string|Array<string>} [options.host=['localhost', 'localhost.qz.io']] Host running the QZ Tray software.
             *  @param {Object} [options.port] Config options for ports to cycle.
             *   @param {Array<number>} [options.port.secure=[8181, 8282, 8383, 8484]] Array of secure (WSS) ports to try
             *   @param {Array<number>} [options.port.insecure=[8182, 8283, 8384, 8485]] Array of insecure (WS) ports to try
             *  @param {boolean} [options.usingSecure=true] If the web socket should try to use secure ports for connecting.
             *  @param {number} [options.keepAlive=60] Seconds between keep-alive pings to keep connection open. Set to 0 to disable.
             *  @param {number} [options.retries=0] Number of times to reconnect before failing.
             *  @param {number} [options.delay=0] Seconds before firing a connection.  Ignored if <code>options.retries</code> is 0.
             *
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.websocket
             */
            connect: function(options) {
                return _qz.tools.promise(function(resolve, reject) {
                    if (_qz.tools.isActive()) {
                        reject(new Error("An open connection with QZ Tray already exists"));
                        return;
                    } else if (_qz.websocket.connection != null) {
                        reject(new Error("The current connection attempt has not returned yet"));
                        return;
                    }

                    if (!_qz.tools.ws) {
                        reject(new Error("WebSocket not supported by this browser"));
                        return;
                    } else if (!_qz.tools.ws.CLOSED || _qz.tools.ws.CLOSED == 2) {
                        reject(new Error("Unsupported WebSocket version detected: HyBi-00/Hixie-76"));
                        return;
                    }

                    //ensure some form of options exists for value checks
                    if (options == undefined) { options = {}; }

                    //disable secure ports if page is not secure
                    if (typeof location === 'undefined' || location.protocol !== 'https:') {
                        //respect forcing secure ports if it is defined, otherwise disable
                        if (typeof options.usingSecure === 'undefined') {
                            _qz.log.trace("Disabling secure ports due to insecure page");
                            options.usingSecure = false;
                        }
                    }

                    //ensure any hosts are passed to internals as an array
                    if (typeof options.host !== 'undefined' && !Array.isArray(options.host)) {
                        options.host = [options.host];
                    }

                    var attempt = function(count) {
                        var tried = false;
                        var nextAttempt = function() {
                            if (!tried) {
                                tried = true;

                                if (options && count < options.retries) {
                                    attempt(count + 1);
                                } else {
                                    _qz.websocket.connection = null;
                                    reject.apply(null, arguments);
                                }
                            }
                        };

                        var delayed = function() {
                            var config = _qz.tools.extend({}, _qz.websocket.connectConfig, options);
                            _qz.websocket.setup.findConnection(config, resolve, nextAttempt);
                        };
                        if (count == 0) {
                            delayed(); // only retries will be called with a delay
                        } else {
                            setTimeout(delayed, options.delay * 1000);
                        }
                    };

                    attempt(0);
                });
            },

            /**
             * Stop any active connection with QZ Tray.
             *
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.websocket
             */
            disconnect: function() {
                return _qz.tools.promise(function(resolve, reject) {
                    if (_qz.tools.isActive()) {
                        _qz.websocket.connection.close();
                        _qz.websocket.connection.promise = { resolve: resolve, reject: reject };
                    } else {
                        reject(new Error("No open connection with QZ Tray"));
                    }
                });
            },

            /**
             * List of functions called for any connections errors outside of an API call.<p/>
             * Also called if {@link websocket#connect} fails to connect.
             *
             * @param {Function|Array<Function>} calls Single or array of <code>Function({Event} event)</code> calls.
             *
             * @memberof qz.websocket
             */
            setErrorCallbacks: function(calls) {
                _qz.websocket.errorCallbacks = calls;
            },

            /**
             * List of functions called for any connection closing event outside of an API call.<p/>
             * Also called when {@link websocket#disconnect} is called.
             *
             * @param {Function|Array<Function>} calls Single or array of <code>Function({Event} event)</code> calls.
             *
             * @memberof qz.websocket
             */
            setClosedCallbacks: function(calls) {
                _qz.websocket.closedCallbacks = calls;
            },

            /**
             * @deprecated Since 2.1.0.  Please use qz.networking.device() instead
             *
             * @param {string} [hostname] Hostname to try to connect to when determining network interfaces, defaults to "google.com"
             * @param {number} [port] Port to use with custom hostname, defaults to 443
             * @param {string} [signature] Pre-signed signature of hashed JSON string containing <code>call='websocket.getNetworkInfo'</code>, <code>params</code> object, and <code>timestamp</code>.
             * @param {number} [signingTimestamp] Required with <code>signature</code>. Timestamp used with pre-signed content.
             *
             * @returns {Promise<Object<{ipAddress: string, macAddress: string}>|Error>} Connected system's network information.
             *
             * @memberof qz.websocket
             */
            getNetworkInfo: _qz.compatible.networking,

            /**
             * @returns {Object<{socket: String, host: String, port: Number}>} Details of active websocket connection
             *
             * @memberof qz.websocket
             */
            getConnectionInfo: function() {
                if (_qz.tools.assertActive()) {
                    var url = _qz.websocket.connection.url.split(/[:\/]+/g);
                    return { socket: url[0], host: url[1], port: +url[2] };
                }
            }
        },


        /**
         * Calls related to getting printer information from the connection.
         * @namespace qz.printers
         */
        printers: {
            /**
             * @param {string} [signature] Pre-signed signature of hashed JSON string containing <code>call='printers.getDefault</code>, <code>params</code>, and <code>timestamp</code>.
             * @param {number} [signingTimestamp] Required with <code>signature</code>. Timestamp used with pre-signed content.
             *
             * @returns {Promise<string|Error>} Name of the connected system's default printer.
             *
             * @memberof qz.printers
             */
            getDefault: function(signature, signingTimestamp) {
                return _qz.websocket.dataPromise('printers.getDefault', null, signature, signingTimestamp);
            },

            /**
             * @param {string} [query] Search for a specific printer. All printers are returned if not provided.
             * @param {string} [signature] Pre-signed signature of hashed JSON string containing <code>call='printers.find'</code>, <code>params</code>, and <code>timestamp</code>.
             * @param {number} [signingTimestamp] Required with <code>signature</code>. Timestamp used with pre-signed content.
             *
             * @returns {Promise<Array<string>|string|Error>} The matched printer name if <code>query</code> is provided.
             *                                                Otherwise an array of printer names found on the connected system.
             *
             * @memberof qz.printers
             */
            find: function(query, signature, signingTimestamp) {
                return _qz.websocket.dataPromise('printers.find', { query: query }, signature, signingTimestamp);
            },

            /**
             * Provides a list, with additional information, for each printer available to QZ.
             *
             * @returns {Promise<Array<Object>|Object|Error>}
             *
             * @memberof qz.printers
             */
            details: function() {
                return _qz.websocket.dataPromise('printers.detail');
            },

            /**
             * Start listening for printer status events, such as paper_jam events.
             * Reported under the ACTION type in the streamEvent on callbacks.
             *
             * @returns {Promise<null|Error>}
             * @since 2.1.0
             *
             * @see qz.printers.setPrinterCallbacks
             *
             * @param {null|string|Array<string>} printers Printer or list of printers to listen to, null listens to all.
             *
             * @memberof qz.printers
             */
            startListening: function(printers) {
                if (!Array.isArray(printers)) {
                    printers = [printers];
                }
                var params = {
                    printerNames: printers
                };
                return _qz.websocket.dataPromise('printers.startListening', params);
            },

            /**
             * Stop listening for printer status actions.
             *
             * @returns {Promise<null|Error>}
             * @since 2.1.0
             *
             * @see qz.printers.setPrinterCallbacks
             *
             * @memberof qz.printers
             */
            stopListening: function() {
                return _qz.websocket.dataPromise('printers.stopListening');
            },

            /**
             * Retrieve current printer status from any active listeners.
             *
             * @returns {Promise<null|Error>}
             * @since 2.1.0
             *
             * @see qz.printers.startListening
             */
            getStatus: function() {
                return _qz.websocket.dataPromise('printers.getStatus');
            },

            /**
             * List of functions called for any printer status change.
             * Event data will contain <code>{string} printerName</code> and <code>{string} status</code> for all types.
             *  For RECEIVE types, <code>{Array} output</code> (in hexadecimal format).
             *  For ERROR types, <code>{string} exception</code>.
             *  For ACTION types, <code>{string} actionType</code>.
             *
             * @param {Function|Array<Function>} calls Single or array of <code>Function({Object} eventData)</code> calls.
             * @since 2.1.0
             *
             * @memberof qz.printers
             */
            setPrinterCallbacks: function(calls) {
                _qz.printers.printerCallbacks = calls;
            }
        },

        /**
         * Calls related to setting up new printer configurations.
         * @namespace qz.configs
         */
        configs: {
            /**
             * Default options used by new configs if not overridden.
             * Setting a value to NULL will use the printer's default options.
             * Updating these will not update the options on any created config.
             *
             * @param {Object} options Default options used by printer configs if not overridden.
             *
             *  @param {Object} [option.bounds=null] Bounding box rectangle.
             *   @param {number} [options.bounds.x=0] Distance from left for bounding box starting corner
             *   @param {number} [options.bounds.y=0] Distance from top for bounding box starting corner
             *   @param {number} [options.bounds.width=0] Width of bounding box
             *   @param {number} [options.bounds.height=0] Height of bounding box
             *  @param {string} [options.colorType='color'] Valid values <code>[color | grayscale | blackwhite]</code>
             *  @param {number} [options.copies=1] Number of copies to be printed.
             *  @param {number|Array<number>|Object|Array<Object>|string} [options.density=0] Pixel density (DPI, DPMM, or DPCM depending on <code>[options.units]</code>).
             *      If provided as an array, uses the first supported density found (or the first entry if none found).
             *      If provided as a string, valid values are <code>[best | draft]</code>, corresponding to highest or lowest reported density respectively.
             *  @param {number} [options.density.cross=0] Asymmetric pixel density for the cross feed direction.
             *  @param {number} [options.density.feed=0] Asymmetric pixel density for the feed direction.
             *  @param {boolean|string} [options.duplex=false] Double sided printing, Can specify duplex style by passing a string value: <code>[one-sided | duplex | long-edge | tumble | short-edge]</code>
             *  @param {number} [options.fallbackDensity=null] Value used when default density value cannot be read, or in cases where reported as "Normal" by the driver, (in DPI, DPMM, or DPCM depending on <code>[options.units]</code>).
             *  @param {string} [options.interpolation='bicubic'] Valid values <code>[bicubic | bilinear | nearest-neighbor]</code>. Controls how images are handled when resized.
             *  @param {string} [options.jobName=null] Name to display in print queue.
             *  @param {boolean} [options.legacy=false] If legacy style printing should be used.
             *  @param {Object|number} [options.margins=0] If just a number is provided, it is used as the margin for all sides.
             *   @param {number} [options.margins.top=0]
             *   @param {number} [options.margins.right=0]
             *   @param {number} [options.margins.bottom=0]
             *   @param {number} [options.margins.left=0]
             *  @param {string} [options.orientation=null] Valid values <code>[portrait | landscape | reverse-landscape]</code>
             *  @param {number} [options.paperThickness=null]
             *  @param {string|number} [options.printerTray=null] Printer tray to pull from. The number N assumes string equivalent of 'Tray N'. Uses printer default if NULL.
             *  @param {boolean} [options.rasterize=false] Whether documents should be rasterized before printing.
             *                                             Specifying <code>[options.density]</code> for PDF print formats will set this to <code>true</code>.
             *  @param {number} [options.rotation=0] Image rotation in degrees.
             *  @param {boolean} [options.scaleContent=true] Scales print content to page size, keeping ratio.
             *  @param {Object} [options.size=null] Paper size.
             *   @param {number} [options.size.width=null] Page width.
             *   @param {number} [options.size.height=null] Page height.
             *  @param {string} [options.units='in'] Page units, applies to paper size, margins, and density. Valid value <code>[in | cm | mm]</code>
             *
             *  @param {boolean} [options.altPrinting=false] Print the specified file using CUPS command line arguments.  Has no effect on Windows.
             *  @param {string} [options.encoding=null] Character set
             *  @param {string} [options.endOfDoc=null]
             *  @param {number} [options.perSpool=1] Number of pages per spool.
             *
             * @memberof qz.configs
             */
            setDefaults: function(options) {
                _qz.tools.extend(_qz.printing.defaultConfig, options);
            },

            /**
             * Creates new printer config to be used in printing.
             *
             * @param {string|object} printer Name of printer. Use object type to specify printing to file or host.
             *  @param {string} [printer.name] Name of printer to send printing.
             *  @param {string} [printer.file] Name of file to send printing.
             *  @param {string} [printer.host] IP address or host name to send printing.
             *  @param {string} [printer.port] Port used by &lt;printer.host>.
             * @param {Object} [options] Override any of the default options for this config only.
             *
             * @returns {Config} The new config.
             *
             * @see configs.setDefaults
             *
             * @memberof qz.configs
             */
            create: function(printer, options) {
                return new Config(printer, options);
            }
        },


        /**
         * Send data to selected config for printing.
         * The promise for this method will resolve when the document has been sent to the printer. Actual printing may not be complete.
         * <p/>
         * Optionally, print requests can be pre-signed:
         * Signed content consists of a JSON object string containing no spacing,
         * following the format of the "call" and "params" keys in the API call, with the addition of a "timestamp" key in milliseconds
         * ex. <code>'{"call":"<callName>","params":{...},"timestamp":1450000000}'</code>
         *
         * @param {Object<Config>|Array<Object<Config>>} configs Previously created config object or objects.
         * @param {Array<Object|string>|Array<Array<Object|string>>} data Array of data being sent to the printer.<br/>
         *      String values are interpreted as <code>{type: 'raw', format: 'command', flavor: 'plain', data: &lt;string>}</code>.
         *  @param {string} data.data
         *  @param {string} data.type Printing type. Valid types are <code>[pixel | raw*]</code>. *Default
         *  @param {string} data.format Format of data type used. *Default per type<p/>
         *      For <code>[pixel]</code> types, valid formats are <code>[html | image* | pdf]</code>.<p/>
         *      For <code>[raw]</code> types, valid formats are <code>[command* | html | image | pdf]</code>.
         *  @param {string} data.flavor Flavor of data format used. *Default per format<p/>
         *      For <code>[command]</code> formats, valid flavors are <code>[base64 | file | hex | plain* | xml]</code>.<p/>
         *      For <code>[html]</code> formats, valid flavors are <code>[file* | plain]</code>.<p/>
         *      For <code>[image]</code> formats, valid flavors are <code>[base64 | file*]</code>.<p/>
         *      For <code>[pdf]</code> formats, valid flavors are <code>[base64 | file*]</code>.
         *  @param {Object} [data.options]
         *   @param {string} [data.options.language] Required with <code>[raw]</code> type + <code>[image]</code> format. Printer language.
         *   @param {number} [data.options.x] Optional with <code>[raw]</code> type + <code>[image]</code> format. The X position of the image.
         *   @param {number} [data.options.y] Optional with <code>[raw]</code> type + <code>[image]</code> format. The Y position of the image.
         *   @param {string|number} [data.options.dotDensity] Optional with <code>[raw]</code> type + <code>[image]</code> format.
         *   @param {number} [data.precision=128] Optional with <code>[raw]</code> type <code>[image]</code> format. Bit precision of the ribbons.
         *   @param {boolean|string|Array<Array<number>>} [data.options.overlay=false] Optional with <code>[raw]</code> type <code>[image]</code> format.
         *       Boolean sets entire layer, string sets mask image, Array sets array of rectangles in format <code>[x1,y1,x2,y2]</code>.
         *   @param {string} [data.options.xmlTag] Required with <code>[xml]</code> flavor. Tag name containing base64 formatted data.
         *   @param {number} [data.options.pageWidth] Optional with <code>[html | pdf]</code> formats. Width of the rendering.
         *       Defaults to paper width.
         *   @param {number} [data.options.pageHeight] Optional with <code>[html | pdf]</code> formats. Height of the rendering.
         *       Defaults to paper height for <code>[pdf]</code>, or auto sized for <code>[html]</code>.
         * @param {...*} [arguments] Additionally three more parameters can be specified:<p/>
         *     <code>{boolean} [resumeOnError=false]</code> Whether the chain should continue printing if it hits an error on one the the prints.<p/>
         *     <code>{string|Array<string>} [signature]</code> Pre-signed signature(s) of the JSON string for containing <code>call</code>, <code>params</code>, and <code>timestamp</code>.<p/>
         *     <code>{number|Array<number>} [signingTimestamps]</code> Required to match with <code>signature</code>. Timestamps for each of the passed pre-signed content.
         *
         * @returns {Promise<null|Error>}
         *
         * @see qz.configs.create
         *
         * @memberof qz
         */
        print: function(configs, data) {
            var resumeOnError = false,
                signatures = [],
                signaturesTimestamps = [];

            //find optional parameters
            if (arguments.length >= 3) {
                if (typeof arguments[2] === 'boolean') {
                    resumeOnError = arguments[2];

                    if (arguments.length >= 5) {
                        signatures = arguments[3];
                        signaturesTimestamps = arguments[4];
                    }
                } else if (arguments.length >= 4) {
                    signatures = arguments[2];
                    signaturesTimestamps = arguments[3];
                }

                //ensure values are arrays for consistency
                if (signatures && !Array.isArray(signatures)) { signatures = [signatures]; }
                if (signaturesTimestamps && !Array.isArray(signaturesTimestamps)) { signaturesTimestamps = [signaturesTimestamps]; }
            }

            if (!Array.isArray(configs)) { configs = [configs]; } //single config -> array of configs
            if (!Array.isArray(data[0])) { data = [data]; } //single data array -> array of data arrays

            //clean up data formatting
            for(var d = 0; d < data.length; d++) {
                _qz.tools.relative(data[d]);
                _qz.compatible.data(data[d]);
            }

            var sendToPrint = function(mapping) {
                var params = {
                    printer: mapping.config.getPrinter(),
                    options: mapping.config.getOptions(),
                    data: mapping.data
                };

                return _qz.websocket.dataPromise('print', params, mapping.signature, mapping.timestamp);
            };

            //chain instead of Promise.all, so resumeOnError can collect each error
            var chain = [];
            for(var i = 0; i < configs.length || i < data.length; i++) {
                (function(i_) {
                    var map = {
                        config: configs[Math.min(i_, configs.length - 1)],
                        data: data[Math.min(i_, data.length - 1)],
                        signature: signatures[i_],
                        timestamp: signaturesTimestamps[i_]
                    };

                    chain.push(function() { return sendToPrint(map) });
                })(i);
            }

            //setup to catch errors if needed
            var fallThrough = null;
            if (resumeOnError) {
                var fallen = [];
                fallThrough = function(err) { fallen.push(err); };

                //final promise to reject any errors as a group
                chain.push(function() {
                    return _qz.tools.promise(function(resolve, reject) {
                        fallen.length ? reject(fallen) : resolve();
                    });
                });
            }

            var last = null;
            chain.reduce(function(sequence, link) {
                last = sequence.catch(fallThrough).then(link); //catch is ignored if fallThrough is null
                return last;
            }, _qz.tools.promise(function(r) { r(); })); //an immediately resolved promise to start off the chain

            //return last promise so users can chain off final action or catch when stopping on error
            return last;
        },


        /**
         * Calls related to interaction with serial ports.
         * @namespace qz.serial
         */
        serial: {
            /**
             * @returns {Promise<Array<string>|Error>} Communication (RS232, COM, TTY) ports available on connected system.
             *
             * @memberof qz.serial
             */
            findPorts: function() {
                return _qz.websocket.dataPromise('serial.findPorts');
            },

            /**
             * List of functions called for any response from open serial ports.
             * Event data will contain <code>{string} portName</code> for all types.
             *  For RECEIVE types, <code>{string} output</code>.
             *  For ERROR types, <code>{string} exception</code>.
             *
             * @param {Function|Array<Function>} calls Single or array of <code>Function({object} streamEvent)</code> calls.
             *
             * @memberof qz.serial
             */
            setSerialCallbacks: function(calls) {
                _qz.serial.serialCallbacks = calls;
            },

            /**
             * Opens a serial port for sending and receiving data
             *
             * @param {string} port Name of serial port to open.
             * @param {Object} [options] Serial port configurations.
             *  @param {number} [options.baudRate=9600] Serial port speed. Set to 0 for auto negotiation.
             *  @param {number} [options.dataBits=8] Serial port data bits. Set to 0 for auto negotiation.
             *  @param {number} [options.stopBits=1] Serial port stop bits. Set to 0 for auto negotiation.
             *  @param {string} [options.parity='NONE'] Serial port parity. Set to AUTO for auto negotiation. Valid values <code>[NONE | EVEN | ODD | MARK | SPACE | AUTO]</code>
             *  @param {string} [options.flowControl='NONE'] Serial port flow control. Set to AUTO for auto negotiation. Valid values <code>[NONE | XONXOFF | XONXOFF_OUT | XONXOFF_IN | RTSCTS | RTSCTS_OUT | RTSCTS_IN | AUTO]</code>
             *  @param {string} [options.encoding='UTF-8'] Character set for communications.
             *  @param {string} [options.start=0x0002] DEPRECATED: Legacy character denoting start of serial response. Use <code>options.rx.start</code> instead.
             *  @param {string} [options.end=0x000D] DEPRECATED: Legacy character denoting end of serial response. Use <code>options.rx.end</code> instead.
             *  @param {number} [options.width] DEPRECATED: Legacy use for fixed-width response serial communication. Use <code>options.rx.width</code> instead.
             *  @param {Object} [options.rx] Serial communications response definitions. If an object is passed but no options are defined, all response data will be sent back as it is received unprocessed.
             *   @param {string|Array<string>} [options.rx.start] Character(s) denoting start of response bytes. Used in conjunction with `end`, `width`, or `lengthbit` property.
             *   @param {string} [options.rx.end] Character denoting end of response bytes. Used in conjunction with `start` property.
             *   @param {number} [options.rx.width] Fixed width size of response bytes (not including header if `start` is set). Used alone or in conjunction with `start` property.
             *   @param {boolean} [options.rx.untilNewline] Returns data between newline characters (`\n` or `\r`) Truncates empty responses.  Overrides `start`, `end`, `width`.
             *   @param {number|Object} [options.rx.lengthBytes] If a number is passed it is treated as the length index. Other values are left as their defaults.
             *    @param {number} [options.rx.lengthBytes.index=0] Position of the response byte (not including response `start` bytes) used to denote the length of the remaining response data.
             *    @param {number} [options.rx.lengthBytes.length=1] Length of response length bytes after response header.
             *    @param {string} [options.rx.lengthBytes.endian='BIG'] Byte endian for multi-byte length values. Valid values <code>[BIG | LITTLE]</code>
             *   @param {number|Object} [options.rx.crcBytes] If a number is passed it is treated as the crc length. Other values are left as their defaults.
             *    @param {number} [options.rx.crcBytes.index=0] Position after the response data (not including length or data bytes) used to denote the crc.
             *    @param {number} [options.rx.crcBytes.length=1] Length of response crc bytes after the response data length.
             *   @param {boolean} [options.rx.includeHeader=false] Whether any of the header bytes (`start` bytes and any length bytes) should be included in the processed response.
             *   @param {string} [options.rx.encoding] Override the encoding used for response data. Uses the same value as <code>options.encoding</code> otherwise.
             *
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.serial
             */
            openPort: function(port, options) {
                var params = {
                    port: port,
                    options: options
                };
                return _qz.websocket.dataPromise('serial.openPort', params);
            },

            /**
             * Send commands over a serial port.
             * Any responses from the device will be sent to serial callback functions.
             *
             * @param {string} port An open serial port to send data.
             * @param {string|Array<string>|Object} data Data to be sent to the serial device.
             *  @param {string} [data.type='PLAIN'] Valid values <code>[FILE | PLAIN | HEX | BASE64]</code>
             *  @param {string|Array<string>} data.data Data to be sent to the serial device.
             * @param {Object} options Serial port configuration updates. See <code>qz.serial.openPort</code> `options` docs for available values.
             *     For best performance, it is recommended to only set these values on the port open call.
             *
             * @returns {Promise<null|Error>}
             *
             * @see qz.serial.setSerialCallbacks
             *
             * @memberof qz.serial
             */
            sendData: function(port, data, options) {
                if (_qz.tools.versionCompare(2, 1, 0, 12) >= 0) {
                    if (typeof data !== 'object') {
                        data = {
                            data: data,
                            type: "PLAIN"
                        };
                    }

                    if (data.type && data.type.toUpperCase() == "FILE") {
                        data.data = _qz.tools.absolute(data.data);
                    }
                }

                var params = {
                    port: port,
                    data: data,
                    options: options
                };
                return _qz.websocket.dataPromise('serial.sendData', params);
            },

            /**
             * @param {string} port Name of port to close.
             *
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.serial
             */
            closePort: function(port) {
                return _qz.websocket.dataPromise('serial.closePort', { port: port });
            }
        },


        /**
         * Calls related to interaction with USB devices.
         * @namespace qz.usb
         */
        usb: {
            /**
             * List of available USB devices. Includes (hexadecimal) vendor ID, (hexadecimal) product ID, and hub status.
             * If supported, also returns manufacturer and product descriptions.
             *
             * @param includeHubs Whether to include USB hubs.
             * @returns {Promise<Array<Object>|Error>} Array of JSON objects containing information on connected USB devices.
             *
             * @memberof qz.usb
             */
            listDevices: function(includeHubs) {
                return _qz.websocket.dataPromise('usb.listDevices', { includeHubs: includeHubs });
            },

            /**
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             * @returns {Promise<Array<string>|Error>} List of available (hexadecimal) interfaces on a USB device.
             *
             * @memberof qz.usb
             */
            listInterfaces: function(deviceInfo) {
                if (typeof deviceInfo !== 'object') { deviceInfo = { vendorId: arguments[0], productId: arguments[1] }; } //backwards compatibility

                return _qz.websocket.dataPromise('usb.listInterfaces', deviceInfo);
            },

            /**
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             *  @param deviceInfo.iface Hex string of interface on the USB device to search.
             * @returns {Promise<Array<string>|Error>} List of available (hexadecimal) endpoints on a USB device's interface.
             *
             * @memberof qz.usb
             */
            listEndpoints: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        interface: arguments[2]
                    };
                }

                return _qz.websocket.dataPromise('usb.listEndpoints', deviceInfo);
            },

            /**
             * List of functions called for any response from open usb devices.
             * Event data will contain <code>{string} vendorId</code> and <code>{string} productId</code> for all types.
             *  For RECEIVE types, <code>{Array} output</code> (in hexadecimal format).
             *  For ERROR types, <code>{string} exception</code>.
             *
             * @param {Function|Array<Function>} calls Single or array of <code>Function({Object} eventData)</code> calls.
             *
             * @memberof qz.usb
             */
            setUsbCallbacks: function(calls) {
                _qz.usb.usbCallbacks = calls;
            },

            /**
             * Claim a USB device's interface to enable sending/reading data across an endpoint.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             *  @param deviceInfo.interface Hex string of interface on the USB device to claim.
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.usb
             */
            claimDevice: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        interface: arguments[2]
                    };
                }

                return _qz.websocket.dataPromise('usb.claimDevice', deviceInfo);
            },

            /**
             * Check the current claim state of a USB device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             * @returns {Promise<boolean|Error>}
             *
             * @since 2.0.2
             * @memberOf qz.usb
             */
            isClaimed: function(deviceInfo) {
                if (typeof deviceInfo !== 'object') { deviceInfo = { vendorId: arguments[0], productId: arguments[1] }; } //backwards compatibility

                return _qz.websocket.dataPromise('usb.isClaimed', deviceInfo);
            },

            /**
             * Send data to a claimed USB device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             *  @param deviceInfo.endpoint Hex string of endpoint on the claimed interface for the USB device.
             *  @param deviceInfo.data Bytes to send over specified endpoint.
             *  @param {string} [deviceInfo.type='PLAIN'] Valid values <code>[FILE | PLAIN | HEX | BASE64]</code>
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.usb
             */
            sendData: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        endpoint: arguments[2],
                        data: arguments[3]
                    };
                }

                if (_qz.tools.versionCompare(2, 1, 0, 12) >= 0) {
                    if (typeof deviceInfo.data !== 'object') {
                        deviceInfo.data = {
                            data: deviceInfo.data,
                            type: "PLAIN"
                        };
                    }

                    if (deviceInfo.data.type && deviceInfo.data.type.toUpperCase() == "FILE") {
                        deviceInfo.data.data = _qz.tools.absolute(deviceInfo.data.data);
                    }
                }

                return _qz.websocket.dataPromise('usb.sendData', deviceInfo);
            },

            /**
             * Read data from a claimed USB device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             *  @param deviceInfo.endpoint Hex string of endpoint on the claimed interface for the USB device.
             *  @param deviceInfo.responseSize Size of the byte array to receive a response in.
             * @returns {Promise<Array<string>|Error>} List of (hexadecimal) bytes received from the USB device.
             *
             * @memberof qz.usb
             */
            readData: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        endpoint: arguments[2],
                        responseSize: arguments[3]
                    };
                }

                return _qz.websocket.dataPromise('usb.readData', deviceInfo);
            },

            /**
             * Provides a continuous stream of read data from a claimed USB device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             *  @param deviceInfo.endpoint Hex string of endpoint on the claimed interface for the USB device.
             *  @param deviceInfo.responseSize Size of the byte array to receive a response in.
             *  @param deviceInfo.interval=100 Frequency to send read data back, in milliseconds.
             * @returns {Promise<null|Error>}
             *
             * @see qz.usb.setUsbCallbacks
             *
             * @memberof qz.usb
             */
            openStream: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        endpoint: arguments[2],
                        responseSize: arguments[3],
                        interval: arguments[4]
                    };
                }

                return _qz.websocket.dataPromise('usb.openStream', deviceInfo);
            },

            /**
             * Stops the stream of read data from a claimed USB device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             *  @param deviceInfo.endpoint Hex string of endpoint on the claimed interface for the USB device.
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.usb
             */
            closeStream: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        endpoint: arguments[2]
                    };
                }

                return _qz.websocket.dataPromise('usb.closeStream', deviceInfo);
            },

            /**
             * Release a claimed USB device to free resources after sending/reading data.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of USB device's vendor ID.
             *  @param deviceInfo.productId Hex string of USB device's product ID.
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.usb
             */
            releaseDevice: function(deviceInfo) {
                if (typeof deviceInfo !== 'object') { deviceInfo = { vendorId: arguments[0], productId: arguments[1] }; } //backwards compatibility

                return _qz.websocket.dataPromise('usb.releaseDevice', deviceInfo);
            }
        },


        /**
         * Calls related to interaction with HID USB devices<br/>
         * Many of these calls can be accomplished from the <code>qz.usb</code> namespace,
         * but HID allows for simpler interaction
         * @namespace qz.hid
         * @since 2.0.1
         */
        hid: {
            /**
             * List of available HID devices. Includes (hexadecimal) vendor ID and (hexadecimal) product ID.
             * If available, also returns manufacturer and product descriptions.
             *
             * @returns {Promise<Array<Object>|Error>} Array of JSON objects containing information on connected HID devices.
             * @since 2.0.1
             *
             * @memberof qz.hid
             */
            listDevices: function() {
                return _qz.websocket.dataPromise('hid.listDevices');
            },

            /**
             * Start listening for HID device actions, such as attach / detach events.
             * Reported under the ACTION type in the streamEvent on callbacks.
             *
             * @returns {Promise<null|Error>}
             * @since 2.0.1
             *
             * @see qz.hid.setHidCallbacks
             *
             * @memberof qz.hid
             */
            startListening: function() {
                return _qz.websocket.dataPromise('hid.startListening');
            },

            /**
             * Stop listening for HID device actions.
             *
             * @returns {Promise<null|Error>}
             * @since 2.0.1
             *
             * @see qz.hid.setHidCallbacks
             *
             * @memberof qz.hid
             */
            stopListening: function() {
                return _qz.websocket.dataPromise('hid.stopListening');
            },

            /**
             * List of functions called for any response from open usb devices.
             * Event data will contain <code>{string} vendorId</code> and <code>{string} productId</code> for all types.
             *  For RECEIVE types, <code>{Array} output</code> (in hexadecimal format).
             *  For ERROR types, <code>{string} exception</code>.
             *  For ACTION types, <code>{string} actionType</code>.
             *
             * @param {Function|Array<Function>} calls Single or array of <code>Function({Object} eventData)</code> calls.
             * @since 2.0.1
             *
             * @memberof qz.hid
             */
            setHidCallbacks: function(calls) {
                _qz.hid.hidCallbacks = calls;
            },

            /**
             * Claim a HID device to enable sending/reading data across.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of HID device's vendor ID.
             *  @param deviceInfo.productId Hex string of HID device's product ID.
             *  @param deviceInfo.usagePage Hex string of HID device's usage page when multiple are present.
             *  @param deviceInfo.serial Serial ID of HID device.
             * @returns {Promise<null|Error>}
             * @since 2.0.1
             *
             * @memberof qz.hid
             */
            claimDevice: function(deviceInfo) {
                if (typeof deviceInfo !== 'object') { deviceInfo = { vendorId: arguments[0], productId: arguments[1] }; } //backwards compatibility

                return _qz.websocket.dataPromise('hid.claimDevice', deviceInfo);
            },

            /**
             * Check the current claim state of a HID device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of HID device's vendor ID.
             *  @param deviceInfo.productId Hex string of HID device's product ID.
             *  @param deviceInfo.usagePage Hex string of HID device's usage page when multiple are present.
             *  @param deviceInfo.serial Serial ID of HID device.
             * @returns {Promise<boolean|Error>}
             *
             * @since 2.0.2
             * @memberOf qz.hid
             */
            isClaimed: function(deviceInfo) {
                if (typeof deviceInfo !== 'object') { deviceInfo = { vendorId: arguments[0], productId: arguments[1] }; } //backwards compatibility

                return _qz.websocket.dataPromise('hid.isClaimed', deviceInfo);
            },

            /**
             * Send data to a claimed HID device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of HID device's vendor ID.
             *  @param deviceInfo.productId Hex string of HID device's product ID.
             *  @param deviceInfo.usagePage Hex string of HID device's usage page when multiple are present.
             *  @param deviceInfo.serial Serial ID of HID device.
             *  @param deviceInfo.data Bytes to send over specified endpoint.
             *  @param deviceInfo.endpoint=0x00 First byte of the data packet signifying the HID report ID.
             *                             Must be 0x00 for devices only supporting a single report.
             *  @param deviceInfo.reportId=0x00 Alias for <code>deviceInfo.endpoint</code>. Not used if endpoint is provided.
             *  @param {string} [deviceInfo.type='PLAIN'] Valid values <code>[FILE | PLAIN | HEX | BASE64]</code>
             * @returns {Promise<null|Error>}
             * @since 2.0.1
             *
             * @memberof qz.hid
             */
            sendData: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        data: arguments[2],
                        endpoint: arguments[3]
                    };
                }

                if (_qz.tools.versionCompare(2, 1, 0, 12) >= 0) {
                    if (typeof deviceInfo.data !== 'object') {
                        deviceInfo.data = {
                            data: deviceInfo.data,
                            type: "PLAIN"
                        };
                    }

                    if (deviceInfo.data.type && deviceInfo.data.type.toUpperCase() == "FILE") {
                        deviceInfo.data.data = _qz.tools.absolute(deviceInfo.data.data);
                    }
                }

                return _qz.websocket.dataPromise('hid.sendData', deviceInfo);
            },

            /**
             * Read data from a claimed HID device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of HID device's vendor ID.
             *  @param deviceInfo.productId Hex string of HID device's product ID.
             *  @param deviceInfo.usagePage Hex string of HID device's usage page when multiple are present.
             *  @param deviceInfo.serial Serial ID of HID device.
             *  @param deviceInfo.responseSize Size of the byte array to receive a response in.
             * @returns {Promise<Array<string>|Error>} List of (hexadecimal) bytes received from the HID device.
             * @since 2.0.1
             *
             * @memberof qz.hid
             */
            readData: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        responseSize: arguments[2]
                    };
                }

                return _qz.websocket.dataPromise('hid.readData', deviceInfo);
            },

            /**
             * Provides a continuous stream of read data from a claimed HID device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of HID device's vendor ID.
             *  @param deviceInfo.productId Hex string of HID device's product ID.
             *  @param deviceInfo.usagePage Hex string of HID device's usage page when multiple are present.
             *  @param deviceInfo.serial Serial ID of HID device.
             *  @param deviceInfo.responseSize Size of the byte array to receive a response in.
             *  @param deviceInfo.interval=100 Frequency to send read data back, in milliseconds.
             * @returns {Promise<null|Error>}
             * @since 2.0.1
             *
             * @see qz.hid.setHidCallbacks
             *
             * @memberof qz.hid
             */
            openStream: function(deviceInfo) {
                //backwards compatibility
                if (typeof deviceInfo !== 'object') {
                    deviceInfo = {
                        vendorId: arguments[0],
                        productId: arguments[1],
                        responseSize: arguments[2],
                        interval: arguments[3]
                    };
                }

                return _qz.websocket.dataPromise('hid.openStream', deviceInfo);
            },

            /**
             * Stops the stream of read data from a claimed HID device.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of HID device's vendor ID.
             *  @param deviceInfo.productId Hex string of HID device's product ID.
             *  @param deviceInfo.usagePage Hex string of HID device's usage page when multiple are present.
             *  @param deviceInfo.serial Serial ID of HID device.
             * @returns {Promise<null|Error>}
             * @since 2.0.1
             *
             * @memberof qz.hid
             */
            closeStream: function(deviceInfo) {
                if (typeof deviceInfo !== 'object') { deviceInfo = { vendorId: arguments[0], productId: arguments[1] }; } //backwards compatibility

                return _qz.websocket.dataPromise('hid.closeStream', deviceInfo);
            },

            /**
             * Release a claimed HID device to free resources after sending/reading data.
             *
             * @param {object} deviceInfo Config details of the HID device.
             *  @param deviceInfo.vendorId Hex string of HID device's vendor ID.
             *  @param deviceInfo.productId Hex string of HID device's product ID.
             *  @param deviceInfo.usagePage Hex string of HID device's usage page when multiple are present.
             *  @param deviceInfo.serial Serial ID of HID device.
             * @returns {Promise<null|Error>}
             * @since 2.0.1
             *
             * @memberof qz.hid
             */
            releaseDevice: function(deviceInfo) {
                if (typeof deviceInfo !== 'object') { deviceInfo = { vendorId: arguments[0], productId: arguments[1] }; } //backwards compatibility

                return _qz.websocket.dataPromise('hid.releaseDevice', deviceInfo);
            }
        },


        /**
         * Calls related to interactions with the filesystem
         * @namespace qz.file
         * @since 2.1
         */
        file: {
            /**
             * List of files available at the given directory.<br/>
             * Due to security reasons, paths are limited to the qz data directory unless overridden via properties file.
             *
             * @param {string} path Relative or absolute directory path. Must reside in qz data directory or a white-listed location.
             * @param {Object} [params] Object containing file access parameters
             *  @param {boolean} [params.sandbox=true] If relative location from root is only available to the certificate's connection, otherwise all connections
             *  @param {boolean} [params.shared=true] If relative location from root is accessible to all users on the system, otherwise just the current user
             * @returns {Promise<Array<String>|Error>} Array of files at the given path
             *
             * @memberof qz.file
             */
            list: function(path, params) {
                var param = _qz.tools.extend({ path: path }, params);
                return _qz.websocket.dataPromise('file.list', param);
            },

            /**
             * Reads contents of file at the given path.<br/>
             * Due to security reasons, paths are limited to the qz data directory unless overridden via properties file.
             *
             * @param {string} path Relative or absolute file path. Must reside in qz data directory or a white-listed location.
             * @param {Object} [params] Object containing file access parameters
             *  @param {boolean} [params.sandbox=true] If relative location from root is only available to the certificate's connection, otherwise all connections
             *  @param {boolean} [params.shared=true] If relative location from root is accessible to all users on the system, otherwise just the current user
             * @returns {Promise<String|Error>} String containing the file contents
             *
             * @memberof qz.file
             */
            read: function(path, params) {
                var param = _qz.tools.extend({ path: path }, params);
                return _qz.websocket.dataPromise('file.read', param);
            },

            /**
             * Writes data to the file at the given path.<br/>
             * Due to security reasons, paths are limited to the qz data directory unless overridden via properties file.
             *
             * @param {string} path Relative or absolute file path. Must reside in qz data directory or a white-listed location.
             * @param {Object} params Object containing file access parameters
             *  @param {string} params.data File data to be written
             *  @param {boolean} [params.sandbox=true] If relative location from root is only available to the certificate's connection, otherwise all connections
             *  @param {boolean} [params.shared=true] If relative location from root is accessible to all users on the system, otherwise just the current user
             *  @param {boolean} [params.append=false] Appends to the end of the file if set, otherwise overwrites existing contents
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.file
             */
            write: function(path, params) {
                var param = _qz.tools.extend({ path: path }, params);
                return _qz.websocket.dataPromise('file.write', param);
            },

            /**
             * Deletes a file at given path.<br/>
             * Due to security reasons, paths are limited to the qz data directory unless overridden via properties file.
             *
             * @param {string} path Relative or absolute file path. Must reside in qz data directory or a white-listed location.
             * @param {Object} [params] Object containing file access parameters
             *  @param {boolean} [params.sandbox=true] If relative location from root is only available to the certificate's connection, otherwise all connections
             *  @param {boolean} [params.shared=true] If relative location from root is accessible to all users on the system, otherwise just the current user
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.file
             */
            remove: function(path, params) {
                var param = _qz.tools.extend({ path: path }, params);
                return _qz.websocket.dataPromise('file.remove', param);
            },

            /**
             * Provides a continuous stream of events (and optionally data) from a local file.
             *
             * @param {string} path Relative or absolute directory path. Must reside in qz data directory or a white-listed location.
             * @param {Object} [params] Object containing file access parameters
             *  @param {boolean} [params.sandbox=true] If relative location from root is only available to the certificate's connection, otherwise all connections
             *  @param {boolean} [params.shared=true] If relative location from root is accessible to all users on the system, otherwise just the current user
             *  @param {Object} [params.listener] If defined, file data will be returned on events
             *   @param {number} [params.listener.bytes=-1] Number of bytes to return or -1 for all
             *   @param {number} [params.listener.lines=-1] Number of lines to return or -1 for all
             *   @param {boolean} [params.listener.reverse] Controls whether data should be returned from the bottom of the file.  Default value is true for line mode and false for byte mode.
             *   @param {string|Array<string>} [params.include] File patterns to match.  Blank values will be ignored.
             *   @param {string|Array<string>} [params.exclude] File patterns to exclude.  Blank values will be ignored.  Takes priority over <code>params.include</code>.
             *   @param {boolean} [params.ignoreCase=true] Whether <code>params.include</code> or <code>params.exclude</code> are case-sensitive.
             * @returns {Promise<null|Error>}
             * @since 2.1.0
             *
             * @see qz.file.setFileCallbacks
             *
             * @memberof qz.file
             */
            startListening: function(path, params) {
                if (params && typeof params.include !== 'undefined' && !Array.isArray(params.include)) {
                    params.include = [params.include];
                }
                if (params && typeof params.exclude !== 'undefined' && !Array.isArray(params.exclude)) {
                    params.exclude = [params.exclude];
                }
                var param = _qz.tools.extend({ path: path }, params);
                return _qz.websocket.dataPromise('file.startListening', param);
            },

            /**
             * Closes listeners with the provided settings. Omitting the path parameter will result in all listeners closing.
             *
             * @param {string} [path] Previously opened directory path of listener to close, or omit to close all.
             * @param {Object} [params] Object containing file access parameters
             *  @param {boolean} [params.sandbox=true] If relative location from root is only available to the certificate's connection, otherwise all connections
             *  @param {boolean} [params.shared=true] If relative location from root is accessible to all users on the system, otherwise just the current user
             * @returns {Promise<null|Error>}
             *
             * @memberof qz.file
             */
            stopListening: function(path, params) {
                var param = _qz.tools.extend({ path: path }, params);
                return _qz.websocket.dataPromise('file.stopListening', param);
            },

            /**
             * List of functions called for any response from a file listener.
             *  For ERROR types event data will contain, <code>{string} message</code>.
             *  For ACTION types event data will contain, <code>{string} file {string} eventType {string} [data]</code>.
             *
             * @param {Function|Array<Function>} calls Single or array of <code>Function({Object} eventData)</code> calls.
             * @since 2.1.0
             *
             * @memberof qz.file
             */
            setFileCallbacks: function(calls) {
                _qz.file.fileCallbacks = calls;
            }
        },

        /**
         * Calls related to networking information
         * @namespace qz.networking
         * @since 2.1.0
         */
        networking: {
            /**
             * @param {string} [hostname] Hostname to try to connect to when determining network interfaces, defaults to "google.com"
             * @param {number} [port] Port to use with custom hostname, defaults to 443
             * @returns {Promise<Object|Error>} Connected system's network information.
             *
             * @memberof qz.networking
             * @since 2.1.0
             */
            device: function(hostname, port) {
                // Wrap 2.0
                if (_qz.tools.isVersion(2, 0)) {
                    return _qz.compatible.networking(hostname, port, null, null, function(data) {
                        return { ip: data.ipAddress, mac: data.macAddress };
                    });
                }
                // Use 2.1
                return _qz.websocket.dataPromise('networking.device', {
                    hostname: hostname,
                    port: port
                });
            },

            /**
             * @param {string} [hostname] Hostname to try to connect to when determining network interfaces, defaults to "google.com"
             * @param {number} [port] Port to use with custom hostname, defaults to 443
             * @returns {Promise<Array<Object>|Error>} Connected system's network information.
             *
             * @memberof qz.networking
             * @since 2.1.0
             */
            devices: function(hostname, port) {
                // Wrap 2.0
                if (_qz.tools.isVersion(2, 0)) {
                    return _qz.compatible.networking(hostname, port, null, null, function(data) {
                        return [{ ip: data.ipAddress, mac: data.macAddress }];
                    });
                }
                // Use 2.1
                return _qz.websocket.dataPromise('networking.devices', {
                    hostname: hostname,
                    port: port
                });
            }
        },


        /**
         * Calls related to signing connection requests.
         * @namespace qz.security
         */
        security: {
            /**
             * Set promise resolver for calls to acquire the site's certificate.
             *
             * @param {Function|Promise<string>} promiseHandler Either a function that will be used as a promise resolver (of format <code>Function({function} resolve, {function}reject)</code>),
             *     or the entire promise, either of which should return the public certificate via their respective <code>resolve</code> call.
             *
             * @memberof qz.security
             */
            setCertificatePromise: function(promiseHandler) {
                _qz.security.certHandler = promiseHandler;
            },

            /**
             * Set promise factory for calls to sign API calls.
             *
             * @param {Function|Promise<string>} promiseFactory Either a function that accepts a string parameter of the data to be signed
             *     and returns a function to be used as a promise resolver (of format <code>Function({function} resolve, {function}reject)</code>),
             *     or a promise that can take a string parameter of the data to be signed, either of which should return the signed contents of
             *     the passed string parameter via their respective <code>resolve</code> call.
             *
             * @example
             *  qz.security.setSignaturePromise(function(dataToSign) {
             *    return function(resolve, reject) {
             *      $.ajax("/signing-url?data=" + dataToSign).then(resolve, reject);
             *    }
             *  })
             *
             * @memberof qz.security
             */
            setSignaturePromise: function(promiseFactory) {
                _qz.security.signatureFactory = promiseFactory;
            },

            /**
             * Set which signing algorithm QZ will check signatures against.
             *
             * @param {string} algorithm The algorithm used in signing. Valid values: <code>[SHA1 | SHA256 | SHA512]</code>
             * @since 2.1.0
             *
             * @memberof qz.security
             */
            setSignatureAlgorithm: function(algorithm) {
                //warn for incompatibilities if known
                if (!_qz.compatible.algorithm()) {
                    return;
                }

                if (["SHA1", "SHA256", "SHA512"].indexOf(algorithm.toUpperCase()) < 0) {
                    _qz.log.error("Signing algorithm '" + algorithm + "' is not supported.");
                } else {
                    _qz.security.signAlgorithm = algorithm;
                }
            },

            /**
             * Get the signing algorithm QZ will be checking signatures against.
             *
             * @returns {string} The algorithm used in signing.
             * @since 2.1.0
             *
             * @memberof qz.security
             */
            getSignatureAlgorithm: function() {
                return _qz.security.signAlgorithm;
            }
        },

        /**
         * Calls related to compatibility adjustments
         * @namespace qz.api
         */
        api: {
            /**
             * Show or hide QZ api debugging statements in the browser console.
             *
             * @param {boolean} show Whether the debugging logs for QZ should be shown. Hidden by default.
             * @returns {boolean} Value of debugging flag
             * @memberof qz.api
             */
            showDebug: function(show) {
                return (_qz.DEBUG = show);
            },

            /**
             * Get version of connected QZ Tray application.
             *
             * @returns {Promise<string|Error>} Version number of QZ Tray.
             *
             * @memberof qz.api
             */
            getVersion: function() {
                return _qz.websocket.dataPromise('getVersion');
            },

            /**
             * Checks for the specified version of connected QZ Tray application.
             *
             * @param {string|number} [major] Major version to check
             * @param {string|number} [minor] Minor version to check
             * @param {string|number} [patch] Patch version to check
             *
             * @memberof qz.api
             */
            isVersion: _qz.tools.isVersion,

            isVersionGreater: function(major, minor, patch, build) {
                return _qz.tools.versionCompare(major, minor, patch, build) > 0;
            },

            isVersionLess: function(major, minor, patch, build) {
                return _qz.tools.versionCompare(major, minor, patch, build) < 0;
            },

            /**
             * Change the promise library used by QZ API.
             * Should be called before any initialization to avoid possible errors.
             *
             * @param {Function} promiser <code>Function({function} resolver)</code> called to create new promises.
             *
             * @memberof qz.api
             */
            setPromiseType: function(promiser) {
                _qz.tools.promise = promiser;
            },

            /**
             * Change the SHA-256 hashing function used by QZ API.
             * Should be called before any initialization to avoid possible errors.
             *
             * @param {Function} hasher <code>Function({function} message)</code> called to create hash of passed string.
             *
             * @memberof qz.api
             */
            setSha256Type: function(hasher) {
                _qz.tools.hash = hasher;
            },

            /**
             * Change the WebSocket handler.
             * Should be called before any initialization to avoid possible errors.
             *
             * @param {Function} ws <code>Function({function} WebSocket)</code> called to override the internal WebSocket handler.
             *
             * @memberof qz.api
             */
            setWebSocketType: function(ws) {
                _qz.tools.ws = ws;
            }
        },

        /**
         * Version of this JavaScript library
         *
         * @constant {string}
         *
         * @memberof qz
         */
        version: _qz.VERSION
    };

    return qz;
})();


(function() {
    {
        module.exports = qz;
        try {
            // var crypto = require('crypto');
            qz.api.setSha256Type(function(data) {
                return crypto.createHash('sha256').update(data).digest('hex');
            });
        }
        catch(ignore) {}
    }
})();
});

/**
 * A JavaScript implementation of the SHA family of hashes, as defined in FIPS PUB 180-4 and FIPS PUB 202, as
 * well as the corresponding HMAC implementation as defined in FIPS PUB 198a
 *
 * Copyright 2008-2020 Brian Turek, 1998-2009 Paul Johnston & Contributors
 * Distributed under the BSD License
 * See http://caligatio.github.com/jsSHA/ for more information
 */
const t="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";function n(t,n,e,r){let i,s,o;const h=n||[0],u=(e=e||0)>>>3,w=-1===r?3:0;for(i=0;i<t.length;i+=1)o=i+u,s=o>>>2,h.length<=s&&h.push(0),h[s]|=t[i]<<8*(w+r*(o%4));return {value:h,binLen:8*t.length+e}}function e(e,r,i){switch(r){case"UTF8":case"UTF16BE":case"UTF16LE":break;default:throw new Error("encoding must be UTF8, UTF16BE, or UTF16LE")}switch(e){case"HEX":return function(t,n,e){return function(t,n,e,r){let i,s,o,h;if(0!=t.length%2)throw new Error("String of HEX type must be in byte increments");const u=n||[0],w=(e=e||0)>>>3,c=-1===r?3:0;for(i=0;i<t.length;i+=2){if(s=parseInt(t.substr(i,2),16),isNaN(s))throw new Error("String of HEX type contains invalid characters");for(h=(i>>>1)+w,o=h>>>2;u.length<=o;)u.push(0);u[o]|=s<<8*(c+r*(h%4));}return {value:u,binLen:4*t.length+e}}(t,n,e,i)};case"TEXT":return function(t,n,e){return function(t,n,e,r,i){let s,o,h,u,w,c,f,a,l=0;const A=e||[0],E=(r=r||0)>>>3;if("UTF8"===n)for(f=-1===i?3:0,h=0;h<t.length;h+=1)for(s=t.charCodeAt(h),o=[],128>s?o.push(s):2048>s?(o.push(192|s>>>6),o.push(128|63&s)):55296>s||57344<=s?o.push(224|s>>>12,128|s>>>6&63,128|63&s):(h+=1,s=65536+((1023&s)<<10|1023&t.charCodeAt(h)),o.push(240|s>>>18,128|s>>>12&63,128|s>>>6&63,128|63&s)),u=0;u<o.length;u+=1){for(c=l+E,w=c>>>2;A.length<=w;)A.push(0);A[w]|=o[u]<<8*(f+i*(c%4)),l+=1;}else for(f=-1===i?2:0,a="UTF16LE"===n&&1!==i||"UTF16LE"!==n&&1===i,h=0;h<t.length;h+=1){for(s=t.charCodeAt(h),!0===a&&(u=255&s,s=u<<8|s>>>8),c=l+E,w=c>>>2;A.length<=w;)A.push(0);A[w]|=s<<8*(f+i*(c%4)),l+=2;}return {value:A,binLen:8*l+r}}(t,r,n,e,i)};case"B64":return function(n,e,r){return function(n,e,r,i){let s,o,h,u,w,c,f,a=0;const l=e||[0],A=(r=r||0)>>>3,E=-1===i?3:0,H=n.indexOf("=");if(-1===n.search(/^[a-zA-Z0-9=+/]+$/))throw new Error("Invalid character in base-64 string");if(n=n.replace(/=/g,""),-1!==H&&H<n.length)throw new Error("Invalid '=' found in base-64 string");for(o=0;o<n.length;o+=4){for(w=n.substr(o,4),u=0,h=0;h<w.length;h+=1)s=t.indexOf(w.charAt(h)),u|=s<<18-6*h;for(h=0;h<w.length-1;h+=1){for(f=a+A,c=f>>>2;l.length<=c;)l.push(0);l[c]|=(u>>>16-8*h&255)<<8*(E+i*(f%4)),a+=1;}}return {value:l,binLen:8*a+r}}(n,e,r,i)};case"BYTES":return function(t,n,e){return function(t,n,e,r){let i,s,o,h;const u=n||[0],w=(e=e||0)>>>3,c=-1===r?3:0;for(s=0;s<t.length;s+=1)i=t.charCodeAt(s),h=s+w,o=h>>>2,u.length<=o&&u.push(0),u[o]|=i<<8*(c+r*(h%4));return {value:u,binLen:8*t.length+e}}(t,n,e,i)};case"ARRAYBUFFER":try{new ArrayBuffer(0);}catch(t){throw new Error("ARRAYBUFFER not supported by this environment")}return function(t,e,r){return function(t,e,r,i){return n(new Uint8Array(t),e,r,i)}(t,e,r,i)};case"UINT8ARRAY":try{new Uint8Array(0);}catch(t){throw new Error("UINT8ARRAY not supported by this environment")}return function(t,e,r){return n(t,e,r,i)};default:throw new Error("format must be HEX, TEXT, B64, BYTES, ARRAYBUFFER, or UINT8ARRAY")}}function r(n,e,r,i){switch(n){case"HEX":return function(t){return function(t,n,e,r){let i,s,o="";const h=n/8,u=-1===e?3:0;for(i=0;i<h;i+=1)s=t[i>>>2]>>>8*(u+e*(i%4)),o+="0123456789abcdef".charAt(s>>>4&15)+"0123456789abcdef".charAt(15&s);return r.outputUpper?o.toUpperCase():o}(t,e,r,i)};case"B64":return function(n){return function(n,e,r,i){let s,o,h,u,w,c="";const f=e/8,a=-1===r?3:0;for(s=0;s<f;s+=3)for(u=s+1<f?n[s+1>>>2]:0,w=s+2<f?n[s+2>>>2]:0,h=(n[s>>>2]>>>8*(a+r*(s%4))&255)<<16|(u>>>8*(a+r*((s+1)%4))&255)<<8|w>>>8*(a+r*((s+2)%4))&255,o=0;o<4;o+=1)c+=8*s+6*o<=e?t.charAt(h>>>6*(3-o)&63):i.b64Pad;return c}(n,e,r,i)};case"BYTES":return function(t){return function(t,n,e){let r,i,s="";const o=n/8,h=-1===e?3:0;for(r=0;r<o;r+=1)i=t[r>>>2]>>>8*(h+e*(r%4))&255,s+=String.fromCharCode(i);return s}(t,e,r)};case"ARRAYBUFFER":try{new ArrayBuffer(0);}catch(t){throw new Error("ARRAYBUFFER not supported by this environment")}return function(t){return function(t,n,e){let r;const i=n/8,s=new ArrayBuffer(i),o=new Uint8Array(s),h=-1===e?3:0;for(r=0;r<i;r+=1)o[r]=t[r>>>2]>>>8*(h+e*(r%4))&255;return s}(t,e,r)};case"UINT8ARRAY":try{new Uint8Array(0);}catch(t){throw new Error("UINT8ARRAY not supported by this environment")}return function(t){return function(t,n,e){let r;const i=n/8,s=-1===e?3:0,o=new Uint8Array(i);for(r=0;r<i;r+=1)o[r]=t[r>>>2]>>>8*(s+e*(r%4))&255;return o}(t,e,r)};default:throw new Error("format must be HEX, B64, BYTES, ARRAYBUFFER, or UINT8ARRAY")}}const i=[1116352408,1899447441,3049323471,3921009573,961987163,1508970993,2453635748,2870763221,3624381080,310598401,607225278,1426881987,1925078388,2162078206,2614888103,3248222580,3835390401,4022224774,264347078,604807628,770255983,1249150122,1555081692,1996064986,2554220882,2821834349,2952996808,3210313671,3336571891,3584528711,113926993,338241895,666307205,773529912,1294757372,1396182291,1695183700,1986661051,2177026350,2456956037,2730485921,2820302411,3259730800,3345764771,3516065817,3600352804,4094571909,275423344,430227734,506948616,659060556,883997877,958139571,1322822218,1537002063,1747873779,1955562222,2024104815,2227730452,2361852424,2428436474,2756734187,3204031479,3329325298],s=[3238371032,914150663,812702999,4144912697,4290775857,1750603025,1694076839,3204075428],o=[1779033703,3144134277,1013904242,2773480762,1359893119,2600822924,528734635,1541459225],h="Chosen SHA variant is not supported";function u(t,n){let e,r;const i=t.binLen>>>3,s=n.binLen>>>3,o=i<<3,h=4-i<<3;if(i%4!=0){for(e=0;e<s;e+=4)r=i+e>>>2,t.value[r]|=n.value[e>>>2]<<o,t.value.push(0),t.value[r+1]|=n.value[e>>>2]>>>h;return (t.value.length<<2)-4>=s+i&&t.value.pop(),{value:t.value,binLen:t.binLen+n.binLen}}return {value:t.value.concat(n.value),binLen:t.binLen+n.binLen}}function w(t){const n={outputUpper:!1,b64Pad:"=",outputLen:-1},e=t||{},r="Output length must be a multiple of 8";if(n.outputUpper=e.outputUpper||!1,e.b64Pad&&(n.b64Pad=e.b64Pad),e.outputLen){if(e.outputLen%8!=0)throw new Error(r);n.outputLen=e.outputLen;}else if(e.shakeLen){if(e.shakeLen%8!=0)throw new Error(r);n.outputLen=e.shakeLen;}if("boolean"!=typeof n.outputUpper)throw new Error("Invalid outputUpper formatting option");if("string"!=typeof n.b64Pad)throw new Error("Invalid b64Pad formatting option");return n}function c(t,n,r,i){const s=t+" must include a value and format";if(!n){if(!i)throw new Error(s);return i}if(void 0===n.value||!n.format)throw new Error(s);return e(n.format,n.encoding||"UTF8",r)(n.value)}class f{constructor(t,n,e){const r=e||{};if(this.t=n,this.i=r.encoding||"UTF8",this.numRounds=r.numRounds||1,isNaN(this.numRounds)||this.numRounds!==parseInt(this.numRounds,10)||1>this.numRounds)throw new Error("numRounds must a integer >= 1");this.s=t,this.o=[],this.h=0,this.u=!1,this.l=0,this.A=!1,this.H=[],this.S=[];}update(t){let n,e=0;const r=this.p>>>5,i=this.m(t,this.o,this.h),s=i.binLen,o=i.value,h=s>>>5;for(n=0;n<h;n+=r)e+this.p<=s&&(this.C=this.R(o.slice(n,n+r),this.C),e+=this.p);this.l+=e,this.o=o.slice(e>>>5),this.h=s%this.p,this.u=!0;}getHash(t,n){let e,i,s=this.U;const o=w(n);if(!0===this.v){if(-1===o.outputLen)throw new Error("Output length must be specified in options");s=o.outputLen;}const h=r(t,s,this.K,o);if(!0===this.A&&this.T)return h(this.T(o));for(i=this.F(this.o.slice(),this.h,this.l,this.g(this.C),s),e=1;e<this.numRounds;e+=1)!0===this.v&&s%32!=0&&(i[i.length-1]&=16777215>>>24-s%32),i=this.F(i,s,0,this.B(this.s),s);return h(i)}setHMACKey(t,n,r){if(!0!==this.L)throw new Error("Variant does not support HMAC");if(!0===this.u)throw new Error("Cannot set MAC key after calling update");const i=e(n,(r||{}).encoding||"UTF8",this.K);this.M(i(t));}M(t){const n=this.p>>>3,e=n/4-1;let r;if(1!==this.numRounds)throw new Error("Cannot set numRounds with MAC");if(!0===this.A)throw new Error("MAC key already set");for(n<t.binLen/8&&(t.value=this.F(t.value,t.binLen,0,this.B(this.s),this.U));t.value.length<=e;)t.value.push(0);for(r=0;r<=e;r+=1)this.H[r]=909522486^t.value[r],this.S[r]=1549556828^t.value[r];this.C=this.R(this.H,this.C),this.l=this.p,this.A=!0;}getHMAC(t,n){const e=w(n);return r(t,this.U,this.K,e)(this.k())}k(){let t;if(!1===this.A)throw new Error("Cannot call getHMAC without first setting MAC key");const n=this.F(this.o.slice(),this.h,this.l,this.g(this.C),this.U);return t=this.R(this.S,this.B(this.s)),t=this.F(n,this.U,this.p,t,this.U),t}}function a(t,n){return t<<n|t>>>32-n}function l(t,n){return t>>>n|t<<32-n}function A(t,n){return t>>>n}function E(t,n,e){return t^n^e}function H(t,n,e){return t&n^~t&e}function S(t,n,e){return t&n^t&e^n&e}function b(t){return l(t,2)^l(t,13)^l(t,22)}function p(t,n){const e=(65535&t)+(65535&n);return (65535&(t>>>16)+(n>>>16)+(e>>>16))<<16|65535&e}function d(t,n,e,r){const i=(65535&t)+(65535&n)+(65535&e)+(65535&r);return (65535&(t>>>16)+(n>>>16)+(e>>>16)+(r>>>16)+(i>>>16))<<16|65535&i}function m(t,n,e,r,i){const s=(65535&t)+(65535&n)+(65535&e)+(65535&r)+(65535&i);return (65535&(t>>>16)+(n>>>16)+(e>>>16)+(r>>>16)+(i>>>16)+(s>>>16))<<16|65535&s}function C(t){return l(t,7)^l(t,18)^A(t,3)}function y(t){return l(t,6)^l(t,11)^l(t,25)}function R(t){return [1732584193,4023233417,2562383102,271733878,3285377520]}function U(t,n){let e,r,i,s,o,h,u;const w=[];for(e=n[0],r=n[1],i=n[2],s=n[3],o=n[4],u=0;u<80;u+=1)w[u]=u<16?t[u]:a(w[u-3]^w[u-8]^w[u-14]^w[u-16],1),h=u<20?m(a(e,5),H(r,i,s),o,1518500249,w[u]):u<40?m(a(e,5),E(r,i,s),o,1859775393,w[u]):u<60?m(a(e,5),S(r,i,s),o,2400959708,w[u]):m(a(e,5),E(r,i,s),o,3395469782,w[u]),o=s,s=i,i=a(r,30),r=e,e=h;return n[0]=p(e,n[0]),n[1]=p(r,n[1]),n[2]=p(i,n[2]),n[3]=p(s,n[3]),n[4]=p(o,n[4]),n}function v(t,n,e,r){let i;const s=15+(n+65>>>9<<4),o=n+e;for(;t.length<=s;)t.push(0);for(t[n>>>5]|=128<<24-n%32,t[s]=4294967295&o,t[s-1]=o/4294967296|0,i=0;i<t.length;i+=16)r=U(t.slice(i,i+16),r);return r}class K extends f{constructor(t,n,r){if("SHA-1"!==t)throw new Error(h);super(t,n,r);const i=r||{};this.L=!0,this.T=this.k,this.K=-1,this.m=e(this.t,this.i,this.K),this.R=U,this.g=function(t){return t.slice()},this.B=R,this.F=v,this.C=[1732584193,4023233417,2562383102,271733878,3285377520],this.p=512,this.U=160,this.v=!1,i.hmacKey&&this.M(c("hmacKey",i.hmacKey,this.K));}}function T(t){let n;return n="SHA-224"==t?s.slice():o.slice(),n}function F(t,n){let e,r,s,o,h,u,w,c,f,a,E;const R=[];for(e=n[0],r=n[1],s=n[2],o=n[3],h=n[4],u=n[5],w=n[6],c=n[7],E=0;E<64;E+=1)R[E]=E<16?t[E]:d(l(U=R[E-2],17)^l(U,19)^A(U,10),R[E-7],C(R[E-15]),R[E-16]),f=m(c,y(h),H(h,u,w),i[E],R[E]),a=p(b(e),S(e,r,s)),c=w,w=u,u=h,h=p(o,f),o=s,s=r,r=e,e=p(f,a);var U;return n[0]=p(e,n[0]),n[1]=p(r,n[1]),n[2]=p(s,n[2]),n[3]=p(o,n[3]),n[4]=p(h,n[4]),n[5]=p(u,n[5]),n[6]=p(w,n[6]),n[7]=p(c,n[7]),n}class g extends f{constructor(t,n,r){if(!1==("SHA-224"===t||"SHA-256"===t))throw new Error(h);super(t,n,r);const i=r||{};this.T=this.k,this.L=!0,this.K=-1,this.m=e(this.t,this.i,this.K),this.R=F,this.g=function(t){return t.slice()},this.B=T,this.F=function(n,e,r,i){return function(t,n,e,r,i){let s,o;const h=15+(n+65>>>9<<4),u=n+e;for(;t.length<=h;)t.push(0);for(t[n>>>5]|=128<<24-n%32,t[h]=4294967295&u,t[h-1]=u/4294967296|0,s=0;s<t.length;s+=16)r=F(t.slice(s,s+16),r);return o="SHA-224"===i?[r[0],r[1],r[2],r[3],r[4],r[5],r[6]]:r,o}(n,e,r,i,t)},this.C=T(t),this.p=512,this.U="SHA-224"===t?224:256,this.v=!1,i.hmacKey&&this.M(c("hmacKey",i.hmacKey,this.K));}}class B{constructor(t,n){this.Y=t,this.N=n;}}function L(t,n){let e;return n>32?(e=64-n,new B(t.N<<n|t.Y>>>e,t.Y<<n|t.N>>>e)):0!==n?(e=32-n,new B(t.Y<<n|t.N>>>e,t.N<<n|t.Y>>>e)):t}function M(t,n){let e;return n<32?(e=32-n,new B(t.Y>>>n|t.N<<e,t.N>>>n|t.Y<<e)):(e=64-n,new B(t.N>>>n|t.Y<<e,t.Y>>>n|t.N<<e))}function k(t,n){return new B(t.Y>>>n,t.N>>>n|t.Y<<32-n)}function Y(t,n,e){return new B(t.Y&n.Y^t.Y&e.Y^n.Y&e.Y,t.N&n.N^t.N&e.N^n.N&e.N)}function N(t){const n=M(t,28),e=M(t,34),r=M(t,39);return new B(n.Y^e.Y^r.Y,n.N^e.N^r.N)}function I(t,n){let e,r;e=(65535&t.N)+(65535&n.N),r=(t.N>>>16)+(n.N>>>16)+(e>>>16);const i=(65535&r)<<16|65535&e;return e=(65535&t.Y)+(65535&n.Y)+(r>>>16),r=(t.Y>>>16)+(n.Y>>>16)+(e>>>16),new B((65535&r)<<16|65535&e,i)}function X(t,n,e,r){let i,s;i=(65535&t.N)+(65535&n.N)+(65535&e.N)+(65535&r.N),s=(t.N>>>16)+(n.N>>>16)+(e.N>>>16)+(r.N>>>16)+(i>>>16);const o=(65535&s)<<16|65535&i;return i=(65535&t.Y)+(65535&n.Y)+(65535&e.Y)+(65535&r.Y)+(s>>>16),s=(t.Y>>>16)+(n.Y>>>16)+(e.Y>>>16)+(r.Y>>>16)+(i>>>16),new B((65535&s)<<16|65535&i,o)}function z(t,n,e,r,i){let s,o;s=(65535&t.N)+(65535&n.N)+(65535&e.N)+(65535&r.N)+(65535&i.N),o=(t.N>>>16)+(n.N>>>16)+(e.N>>>16)+(r.N>>>16)+(i.N>>>16)+(s>>>16);const h=(65535&o)<<16|65535&s;return s=(65535&t.Y)+(65535&n.Y)+(65535&e.Y)+(65535&r.Y)+(65535&i.Y)+(o>>>16),o=(t.Y>>>16)+(n.Y>>>16)+(e.Y>>>16)+(r.Y>>>16)+(i.Y>>>16)+(s>>>16),new B((65535&o)<<16|65535&s,h)}function x(t,n){return new B(t.Y^n.Y,t.N^n.N)}function _(t){const n=M(t,19),e=M(t,61),r=k(t,6);return new B(n.Y^e.Y^r.Y,n.N^e.N^r.N)}function O(t){const n=M(t,1),e=M(t,8),r=k(t,7);return new B(n.Y^e.Y^r.Y,n.N^e.N^r.N)}function P(t){const n=M(t,14),e=M(t,18),r=M(t,41);return new B(n.Y^e.Y^r.Y,n.N^e.N^r.N)}const V=[new B(i[0],3609767458),new B(i[1],602891725),new B(i[2],3964484399),new B(i[3],2173295548),new B(i[4],4081628472),new B(i[5],3053834265),new B(i[6],2937671579),new B(i[7],3664609560),new B(i[8],2734883394),new B(i[9],1164996542),new B(i[10],1323610764),new B(i[11],3590304994),new B(i[12],4068182383),new B(i[13],991336113),new B(i[14],633803317),new B(i[15],3479774868),new B(i[16],2666613458),new B(i[17],944711139),new B(i[18],2341262773),new B(i[19],2007800933),new B(i[20],1495990901),new B(i[21],1856431235),new B(i[22],3175218132),new B(i[23],2198950837),new B(i[24],3999719339),new B(i[25],766784016),new B(i[26],2566594879),new B(i[27],3203337956),new B(i[28],1034457026),new B(i[29],2466948901),new B(i[30],3758326383),new B(i[31],168717936),new B(i[32],1188179964),new B(i[33],1546045734),new B(i[34],1522805485),new B(i[35],2643833823),new B(i[36],2343527390),new B(i[37],1014477480),new B(i[38],1206759142),new B(i[39],344077627),new B(i[40],1290863460),new B(i[41],3158454273),new B(i[42],3505952657),new B(i[43],106217008),new B(i[44],3606008344),new B(i[45],1432725776),new B(i[46],1467031594),new B(i[47],851169720),new B(i[48],3100823752),new B(i[49],1363258195),new B(i[50],3750685593),new B(i[51],3785050280),new B(i[52],3318307427),new B(i[53],3812723403),new B(i[54],2003034995),new B(i[55],3602036899),new B(i[56],1575990012),new B(i[57],1125592928),new B(i[58],2716904306),new B(i[59],442776044),new B(i[60],593698344),new B(i[61],3733110249),new B(i[62],2999351573),new B(i[63],3815920427),new B(3391569614,3928383900),new B(3515267271,566280711),new B(3940187606,3454069534),new B(4118630271,4000239992),new B(116418474,1914138554),new B(174292421,2731055270),new B(289380356,3203993006),new B(460393269,320620315),new B(685471733,587496836),new B(852142971,1086792851),new B(1017036298,365543100),new B(1126000580,2618297676),new B(1288033470,3409855158),new B(1501505948,4234509866),new B(1607167915,987167468),new B(1816402316,1246189591)];function Z(t){let n;return n="SHA-384"===t?[new B(3418070365,s[0]),new B(1654270250,s[1]),new B(2438529370,s[2]),new B(355462360,s[3]),new B(1731405415,s[4]),new B(41048885895,s[5]),new B(3675008525,s[6]),new B(1203062813,s[7])]:[new B(o[0],4089235720),new B(o[1],2227873595),new B(o[2],4271175723),new B(o[3],1595750129),new B(o[4],2917565137),new B(o[5],725511199),new B(o[6],4215389547),new B(o[7],327033209)],n}function j(t,n){let e,r,i,s,o,h,u,w,c,f,a,l;const A=[];for(e=n[0],r=n[1],i=n[2],s=n[3],o=n[4],h=n[5],u=n[6],w=n[7],a=0;a<80;a+=1)a<16?(l=2*a,A[a]=new B(t[l],t[l+1])):A[a]=X(_(A[a-2]),A[a-7],O(A[a-15]),A[a-16]),c=z(w,P(o),(H=h,S=u,new B((E=o).Y&H.Y^~E.Y&S.Y,E.N&H.N^~E.N&S.N)),V[a],A[a]),f=I(N(e),Y(e,r,i)),w=u,u=h,h=o,o=I(s,c),s=i,i=r,r=e,e=I(c,f);var E,H,S;return n[0]=I(e,n[0]),n[1]=I(r,n[1]),n[2]=I(i,n[2]),n[3]=I(s,n[3]),n[4]=I(o,n[4]),n[5]=I(h,n[5]),n[6]=I(u,n[6]),n[7]=I(w,n[7]),n}class q extends f{constructor(t,n,r){if(!1==("SHA-384"===t||"SHA-512"===t))throw new Error(h);super(t,n,r);const i=r||{};this.T=this.k,this.L=!0,this.K=-1,this.m=e(this.t,this.i,this.K),this.R=j,this.g=function(t){return t.slice()},this.B=Z,this.F=function(n,e,r,i){return function(t,n,e,r,i){let s,o;const h=31+(n+129>>>10<<5),u=n+e;for(;t.length<=h;)t.push(0);for(t[n>>>5]|=128<<24-n%32,t[h]=4294967295&u,t[h-1]=u/4294967296|0,s=0;s<t.length;s+=32)r=j(t.slice(s,s+32),r);return o="SHA-384"===i?[(r=r)[0].Y,r[0].N,r[1].Y,r[1].N,r[2].Y,r[2].N,r[3].Y,r[3].N,r[4].Y,r[4].N,r[5].Y,r[5].N]:[r[0].Y,r[0].N,r[1].Y,r[1].N,r[2].Y,r[2].N,r[3].Y,r[3].N,r[4].Y,r[4].N,r[5].Y,r[5].N,r[6].Y,r[6].N,r[7].Y,r[7].N],o}(n,e,r,i,t)},this.C=Z(t),this.p=1024,this.U="SHA-384"===t?384:512,this.v=!1,i.hmacKey&&this.M(c("hmacKey",i.hmacKey,this.K));}}const D=[new B(0,1),new B(0,32898),new B(2147483648,32906),new B(2147483648,2147516416),new B(0,32907),new B(0,2147483649),new B(2147483648,2147516545),new B(2147483648,32777),new B(0,138),new B(0,136),new B(0,2147516425),new B(0,2147483658),new B(0,2147516555),new B(2147483648,139),new B(2147483648,32905),new B(2147483648,32771),new B(2147483648,32770),new B(2147483648,128),new B(0,32778),new B(2147483648,2147483658),new B(2147483648,2147516545),new B(2147483648,32896),new B(0,2147483649),new B(2147483648,2147516424)],G=[[0,36,3,41,18],[1,44,10,45,2],[62,6,43,15,61],[28,55,25,21,56],[27,20,39,8,14]];function J(t){let n;const e=[];for(n=0;n<5;n+=1)e[n]=[new B(0,0),new B(0,0),new B(0,0),new B(0,0),new B(0,0)];return e}function Q(t){let n;const e=[];for(n=0;n<5;n+=1)e[n]=t[n].slice();return e}function W(t,n){let e,r,i,s;const o=[],h=[];if(null!==t)for(r=0;r<t.length;r+=2)n[(r>>>1)%5][(r>>>1)/5|0]=x(n[(r>>>1)%5][(r>>>1)/5|0],new B(t[r+1],t[r]));for(e=0;e<24;e+=1){for(s=J(),r=0;r<5;r+=1)o[r]=(u=n[r][0],w=n[r][1],c=n[r][2],f=n[r][3],a=n[r][4],new B(u.Y^w.Y^c.Y^f.Y^a.Y,u.N^w.N^c.N^f.N^a.N));for(r=0;r<5;r+=1)h[r]=x(o[(r+4)%5],L(o[(r+1)%5],1));for(r=0;r<5;r+=1)for(i=0;i<5;i+=1)n[r][i]=x(n[r][i],h[r]);for(r=0;r<5;r+=1)for(i=0;i<5;i+=1)s[i][(2*r+3*i)%5]=L(n[r][i],G[r][i]);for(r=0;r<5;r+=1)for(i=0;i<5;i+=1)n[r][i]=x(s[r][i],new B(~s[(r+1)%5][i].Y&s[(r+2)%5][i].Y,~s[(r+1)%5][i].N&s[(r+2)%5][i].N));n[0][0]=x(n[0][0],D[e]);}var u,w,c,f,a;return n}function $(t){let n,e,r=0;const i=[0,0],s=[4294967295&t,t/4294967296&2097151];for(n=6;n>=0;n--)e=s[n>>2]>>>8*n&255,0===e&&0===r||(i[r+1>>2]|=e<<8*(r+1),r+=1);return r=0!==r?r:1,i[0]|=r,{value:r+1>4?i:[i[0]],binLen:8+8*r}}function tt(t){return u($(t.binLen),t)}function nt(t,n){let e,r=$(n);r=u(r,t);const i=n>>>2,s=(i-r.value.length%i)%i;for(e=0;e<s;e++)r.value.push(0);return r.value}class et extends f{constructor(t,n,r){let i=6,s=0;super(t,n,r);const o=r||{};if(1!==this.numRounds){if(o.kmacKey||o.hmacKey)throw new Error("Cannot set numRounds with MAC");if("CSHAKE128"===this.s||"CSHAKE256"===this.s)throw new Error("Cannot set numRounds for CSHAKE variants")}switch(this.K=1,this.m=e(this.t,this.i,this.K),this.R=W,this.g=Q,this.B=J,this.C=J(),this.v=!1,t){case"SHA3-224":this.p=s=1152,this.U=224,this.L=!0,this.T=this.k;break;case"SHA3-256":this.p=s=1088,this.U=256,this.L=!0,this.T=this.k;break;case"SHA3-384":this.p=s=832,this.U=384,this.L=!0,this.T=this.k;break;case"SHA3-512":this.p=s=576,this.U=512,this.L=!0,this.T=this.k;break;case"SHAKE128":i=31,this.p=s=1344,this.U=-1,this.v=!0,this.L=!1,this.T=null;break;case"SHAKE256":i=31,this.p=s=1088,this.U=-1,this.v=!0,this.L=!1,this.T=null;break;case"KMAC128":i=4,this.p=s=1344,this.I(r),this.U=-1,this.v=!0,this.L=!1,this.T=this.X;break;case"KMAC256":i=4,this.p=s=1088,this.I(r),this.U=-1,this.v=!0,this.L=!1,this.T=this.X;break;case"CSHAKE128":this.p=s=1344,i=this._(r),this.U=-1,this.v=!0,this.L=!1,this.T=null;break;case"CSHAKE256":this.p=s=1088,i=this._(r),this.U=-1,this.v=!0,this.L=!1,this.T=null;break;default:throw new Error(h)}this.F=function(t,n,e,r,o){return function(t,n,e,r,i,s,o){let h,u,w=0;const c=[],f=i>>>5,a=n>>>5;for(h=0;h<a&&n>=i;h+=f)r=W(t.slice(h,h+f),r),n-=i;for(t=t.slice(h),n%=i;t.length<f;)t.push(0);for(h=n>>>3,t[h>>2]^=s<<h%4*8,t[f-1]^=2147483648,r=W(t,r);32*c.length<o&&(u=r[w%5][w/5|0],c.push(u.N),!(32*c.length>=o));)c.push(u.Y),w+=1,0==64*w%i&&(W(null,r),w=0);return c}(t,n,0,r,s,i,o)},o.hmacKey&&this.M(c("hmacKey",o.hmacKey,this.K));}_(t,n){const e=function(t){const n=t||{};return {funcName:c("funcName",n.funcName,1,{value:[],binLen:0}),customization:c("Customization",n.customization,1,{value:[],binLen:0})}}(t||{});n&&(e.funcName=n);const r=u(tt(e.funcName),tt(e.customization));if(0!==e.customization.binLen||0!==e.funcName.binLen){const t=nt(r,this.p>>>3);for(let n=0;n<t.length;n+=this.p>>>5)this.C=this.R(t.slice(n,n+(this.p>>>5)),this.C),this.l+=this.p;return 4}return 31}I(t){const n=function(t){const n=t||{};return {kmacKey:c("kmacKey",n.kmacKey,1),funcName:{value:[1128353099],binLen:32},customization:c("Customization",n.customization,1,{value:[],binLen:0})}}(t||{});this._(t,n.funcName);const e=nt(tt(n.kmacKey),this.p>>>3);for(let t=0;t<e.length;t+=this.p>>>5)this.C=this.R(e.slice(t,t+(this.p>>>5)),this.C),this.l+=this.p;this.A=!0;}X(t){const n=u({value:this.o.slice(),binLen:this.h},function(t){let n,e,r=0;const i=[0,0],s=[4294967295&t,t/4294967296&2097151];for(n=6;n>=0;n--)e=s[n>>2]>>>8*n&255,0===e&&0===r||(i[r>>2]|=e<<8*r,r+=1);return r=0!==r?r:1,i[r>>2]|=r<<8*r,{value:r+1>4?i:[i[0]],binLen:8+8*r}}(t.outputLen));return this.F(n.value,n.binLen,this.l,this.g(this.C),t.outputLen)}}class JsSHA{constructor(t,n,e){if("SHA-1"==t)this.O=new K(t,n,e);else if("SHA-224"==t||"SHA-256"==t)this.O=new g(t,n,e);else if("SHA-384"==t||"SHA-512"==t)this.O=new q(t,n,e);else {if("SHA3-224"!=t&&"SHA3-256"!=t&&"SHA3-384"!=t&&"SHA3-512"!=t&&"SHAKE128"!=t&&"SHAKE256"!=t&&"CSHAKE128"!=t&&"CSHAKE256"!=t&&"KMAC128"!=t&&"KMAC256"!=t)throw new Error(h);this.O=new et(t,n,e);}}update(t){this.O.update(t);}getHash(t,n){return this.O.getHash(t,n)}setHMACKey(t,n,e){this.O.setHMACKey(t,n,e);}getHMAC(t,n){return this.O.getHMAC(t,n)}}

var nullFn = function () { return null; };
var nullPromise = function () { return Promise.resolve(null); };
var printerContext = React.createContext({
    enabled: false,
    print: nullPromise,
    listPrinters: nullPromise,
    getPrinter: nullFn,
    setPrinter: nullFn,
    setDefaultPrinter: nullPromise,
    isActive: function () { return false; },
    getConnectionInfo: nullPromise,
    connect: nullPromise,
    disconnect: nullPromise,
    isConnected: false,
    encoder: null
});

/* eslint-env browser */
function fetchCertificate(endpoint) {
    return __awaiter(this, void 0, void 0, function () {
        var res;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!endpoint) return [3 /*break*/, 2];
                    return [4 /*yield*/, fetch(endpoint)];
                case 1:
                    res = _a.sent();
                    return [2 /*return*/, res.text()];
                case 2: return [2 /*return*/, ''];
            }
        });
    });
}
function signData(endpoint, toSign) {
    return __awaiter(this, void 0, void 0, function () {
        var res;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!endpoint) return [3 /*break*/, 2];
                    return [4 /*yield*/, fetch(endpoint, {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json;charset=UTF-8'
                            },
                            body: JSON.stringify({ toSign: toSign })
                        })];
                case 1:
                    res = _a.sent();
                    return [2 /*return*/, res.json()];
                case 2: return [2 /*return*/, {
                        data: toSign,
                        signature: ''
                    }];
            }
        });
    });
}
function qzRSASetup(qz, certEndpoint, signEndpoint) {
    qz.security.setCertificatePromise(function (resolve, reject) {
        fetchCertificate(certEndpoint)
            .then(function (cert) { return resolve(cert); })
            .catch(reject);
    });
    qz.security.setSignatureAlgorithm('SHA512');
    qz.security.setSignaturePromise(function (toSign) { return function (resolve, reject) {
        signData(signEndpoint, toSign)
            .then(function (sign) { return resolve(sign.signature); })
            .catch(reject);
    }; });
}

/**
 * ESC/POS _ (Constants)
 */
const _$1 = {
  LF: '\x0a',
  FS: '\x1c',
  FF: '\x0c',
  GS: '\x1d',
  DLE: '\x10',
  EOT: '\x04',
  NUL: '\x00',
  ESC: '\x1b',
  EOL: '\n'
};


/**
 * [FEED_CONTROL_SEQUENCES Feed control sequences]
 * @type {Object}
 */
_$1.FEED_CONTROL_SEQUENCES = {
  CTL_LF: '\x0a',   // Print and line feed
  CTL_GLF: '\x4a\x00',   // Print and feed paper (without spaces between lines)
  CTL_FF: '\x0c',   // Form feed
  CTL_CR: '\x0d',   // Carriage return
  CTL_HT: '\x09',   // Horizontal tab
  CTL_VT: '\x0b',   // Vertical tab
};

_$1.CHARACTER_SPACING = {
  CS_DEFAULT: '\x1b\x20\x00',
  CS_SET: '\x1b\x20'
};

_$1.LINE_SPACING = {
  LS_DEFAULT: '\x1b\x32',
  LS_SET: '\x1b\x33'
};

/**
 * [HARDWARE Printer hardware]
 * @type {Object}
 */
_$1.HARDWARE = {
  HW_INIT: '\x1b\x40', // Clear data in buffer and reset modes
  HW_SELECT: '\x1b\x3d\x01', // Printer select
  HW_RESET: '\x1b\x3f\x0a\x00', // Reset printer hardware
};

/**
 * [CASH_DRAWER Cash Drawer]
 * @type {Object}
 */
_$1.CASH_DRAWER = {
  CD_KICK_2: '\x1b\x70\x00', // Sends a pulse to pin 2 []
  CD_KICK_5: '\x1b\x70\x01', // Sends a pulse to pin 5 []
};

/**
 * [MARGINS Margins sizes]
 * @type {Object}
 */
_$1.MARGINS = {
  BOTTOM: '\x1b\x4f', // Fix bottom size
  LEFT: '\x1b\x6c', // Fix left size
  RIGHT: '\x1b\x51', // Fix right size
};

/**
 * [PAPER Paper]
 * @type {Object}
 */
_$1.PAPER = {
  PAPER_FULL_CUT: '\x1d\x56\x00', // Full cut paper
  PAPER_PART_CUT: '\x1d\x56\x01', // Partial cut paper
  PAPER_CUT_A: '\x1d\x56\x41', // Partial cut paper
  PAPER_CUT_B: '\x1d\x56\x42', // Partial cut paper
};

/**
 * [TEXT_FORMAT Text format]
 * @type {Object}
 */
_$1.TEXT_FORMAT = {

  TXT_NORMAL: '\x1b\x21\x00', // Normal text
  TXT_2HEIGHT: '\x1b\x21\x10', // Double height text
  TXT_2WIDTH: '\x1b\x21\x20', // Double width text
  TXT_4SQUARE: '\x1b\x21\x30', // Double width & height text

  TXT_CUSTOM_SIZE: function (width, height) { // other sizes
    var widthDec = (width - 1) * 16;
    var heightDec = height - 1;
    var sizeDec = widthDec + heightDec;
    return '\x1d\x21' + String.fromCharCode(sizeDec);
  },

  TXT_HEIGHT: {
    1: '\x00',
    2: '\x01',
    3: '\x02',
    4: '\x03',
    5: '\x04',
    6: '\x05',
    7: '\x06',
    8: '\x07'
  },
  TXT_WIDTH: {
    1: '\x00',
    2: '\x10',
    3: '\x20',
    4: '\x30',
    5: '\x40',
    6: '\x50',
    7: '\x60',
    8: '\x70'
  },

  TXT_UNDERL_OFF: '\x1b\x2d\x00', // Underline font OFF
  TXT_UNDERL_ON: '\x1b\x2d\x01', // Underline font 1-dot ON
  TXT_UNDERL2_ON: '\x1b\x2d\x02', // Underline font 2-dot ON
  TXT_BOLD_OFF: '\x1b\x45\x00', // Bold font OFF
  TXT_BOLD_ON: '\x1b\x45\x01', // Bold font ON
  TXT_ITALIC_OFF: '\x1b\x35', // Italic font ON
  TXT_ITALIC_ON: '\x1b\x34', // Italic font ON

  TXT_FONT_A: '\x1b\x4d\x00', // Font type A
  TXT_FONT_B: '\x1b\x4d\x01', // Font type B
  TXT_FONT_C: '\x1b\x4d\x02', // Font type C

  TXT_ALIGN_LT: '\x1b\x61\x00', // Left justification
  TXT_ALIGN_CT: '\x1b\x61\x01', // Centering
  TXT_ALIGN_RT: '\x1b\x61\x02', // Right justification
};

/**
 * Qsprinter-compatible
 * Added by Attawit Kittikrairit
 * [MODEL Model-specific commands]
 * @type {Object}
 */
_$1.MODEL = {
  QSPRINTER: {
    BARCODE_MODE: {
      ON: '\x1d\x45\x43\x01', // Barcode mode on
      OFF: '\x1d\x45\x43\x00', // Barcode mode off
    },
    BARCODE_HEIGHT_DEFAULT: '\x1d\x68\xA2', // Barcode height default:162
    CODE2D_FORMAT: {
      PIXEL_SIZE: {
        CMD: '\x1b\x23\x23\x51\x50\x49\x58',
        MIN: 1,
        MAX: 24,
        DEFAULT: 12,
      },
      VERSION: {
        CMD: '\x1d\x28\x6b\x03\x00\x31\x43',
        MIN: 1,
        MAX: 16,
        DEFAULT: 3,
      },
      LEVEL: {
        CMD: '\x1d\x28\x6b\x03\x00\x31\x45',
        OPTIONS: {
          L: 48,
          M: 49,
          Q: 50,
          H: 51,
        }
      },
      LEN_OFFSET: 3,
      SAVEBUF: {
        // Format: CMD_P1{LEN_2BYTE}CMD_P2{DATA}
        // DATA Max Length: 256*256 - 3 (65533)
        CMD_P1: '\x1d\x28\x6b',
        CMD_P2: '\x31\x50\x30',
      },
      PRINTBUF: {
        // Format: CMD_P1{LEN_2BYTE}CMD_P2
        CMD_P1: '\x1d\x28\x6b',
        CMD_P2: '\x31\x51\x30',
      }
    },
  },
};

/**
 * [BARCODE_FORMAT Barcode format]
 * @type {Object}
 */
_$1.BARCODE_FORMAT = {
  BARCODE_TXT_OFF: '\x1d\x48\x00', // HRI barcode chars OFF
  BARCODE_TXT_ABV: '\x1d\x48\x01', // HRI barcode chars above
  BARCODE_TXT_BLW: '\x1d\x48\x02', // HRI barcode chars below
  BARCODE_TXT_BTH: '\x1d\x48\x03', // HRI barcode chars both above and below

  BARCODE_FONT_A: '\x1d\x66\x00', // Font type A for HRI barcode chars
  BARCODE_FONT_B: '\x1d\x66\x01', // Font type B for HRI barcode chars

  BARCODE_HEIGHT: function (height) { // Barcode Height [1-255]
    return '\x1d\x68' + String.fromCharCode(height);
  },
  // Barcode Width  [2-6]
  BARCODE_WIDTH: {
    1: '\x1d\x77\x02',
    2: '\x1d\x77\x03',
    3: '\x1d\x77\x04',
    4: '\x1d\x77\x05',
    5: '\x1d\x77\x06',
  },
  BARCODE_HEIGHT_DEFAULT: '\x1d\x68\x64', // Barcode height default:100
  BARCODE_WIDTH_DEFAULT: '\x1d\x77\x01', // Barcode width default:1

  BARCODE_UPC_A: '\x1d\x6b\x00', // Barcode type UPC-A
  BARCODE_UPC_E: '\x1d\x6b\x01', // Barcode type UPC-E
  BARCODE_EAN13: '\x1d\x6b\x02', // Barcode type EAN13
  BARCODE_EAN8: '\x1d\x6b\x03', // Barcode type EAN8
  BARCODE_CODE39: '\x1d\x6b\x04', // Barcode type CODE39
  BARCODE_ITF: '\x1d\x6b\x05', // Barcode type ITF
  BARCODE_NW7: '\x1d\x6b\x06', // Barcode type NW7
  BARCODE_CODE93: '\x1d\x6b\x48', // Barcode type CODE93
  BARCODE_CODE128: '\x1d\x6b\x49', // Barcode type CODE128
};

/**
 * [CODE2D_FORMAT description]
 * @type {Object}
 */
_$1.CODE2D_FORMAT = {
  TYPE_PDF417: _$1.GS + 'Z' + '\x00',
  TYPE_DATAMATRIX: _$1.GS + 'Z' + '\x01',
  TYPE_QR: _$1.GS + 'Z' + '\x02',
  CODE2D: _$1.ESC + 'Z',
  QR_LEVEL_L: 'L', // correct level 7%
  QR_LEVEL_M: 'M', // correct level 15%
  QR_LEVEL_Q: 'Q', // correct level 25%
  QR_LEVEL_H: 'H'  // correct level 30%
};

/**
 * [IMAGE_FORMAT Image format]
 * @type {Object}
 */
_$1.IMAGE_FORMAT = {
  S_RASTER_N: '\x1d\x76\x30\x00', // Set raster image normal size
  S_RASTER_2W: '\x1d\x76\x30\x01', // Set raster image double width
  S_RASTER_2H: '\x1d\x76\x30\x02', // Set raster image double height
  S_RASTER_Q: '\x1d\x76\x30\x03', // Set raster image quadruple
};

/**
 * [BITMAP_FORMAT description]
 * @type {Object}
 */
_$1.BITMAP_FORMAT = {
  BITMAP_S8: '\x1b\x2a\x00',
  BITMAP_D8: '\x1b\x2a\x01',
  BITMAP_S24: '\x1b\x2a\x20',
  BITMAP_D24: '\x1b\x2a\x21'
};

/**
 * [GSV0_FORMAT description]
 * @type {Object}
 */
_$1.GSV0_FORMAT = {
  GSV0_NORMAL: '\x1d\x76\x30\x00',
  GSV0_DW: '\x1d\x76\x30\x01',
  GSV0_DH: '\x1d\x76\x30\x02',
  GSV0_DWDH: '\x1d\x76\x30\x03'
};

/**
 * [BEEP description]
 * @type {string}
 */
_$1.BEEP = '\x1b\x42', // Printer Buzzer pre hex

/**
 * [COLOR description]
 * @type {Object}
 */
_$1.COLOR = {
  0: '\x1b\x72\x00', // black
  1: '\x1b\x72\x01', // red
  REVERSE: '\x1dB1', // Reverses the colors - white text on black background
  UNREVERSE: '\x1dB0' // Default: undo the reverse - black text on white background
};

/**
 * [SCREEN description]
 * @type {Object}
 */
_$1.SCREEN = {
  BS: '\x08', // Moves the cursor one character position to the left
  HT: '\x09', // Moves the cursor one character position to the right
  LF: '\x0a', // Moves the cursor down one line
  US_LF: '\x1f\x0a', // Moves the cursor up one line
  HOM: '\x0b', // Moves the cursor to the left-most position on the upper line (home position)
  CR: '\x0d', // Moves the cursor to the left-most position on the current line
  US_CR: '\x1f\x0d', // Moves the cursor to the right-most position on the current line
  US_B: '\x1f\x42', // Moves the cursor to the bottom position
  US_$: '\x1f\x24', // Moves the cursor to the nth position on the mth line
  CLR: '\x0c', // Clears all displayed characters
  CAN: '\x18', // Clears the line containing the cursor
  US_MD1: '\x1f\x01', // Selects overwrite mode as the screen display mode
  US_MD2: '\x1f\x02', // Selects vertical scroll mode as the screen display mode
  US_MD3: '\x1f\x03', // Selects horizontal scroll mode as the display screen mode
  US_C: '\x1f\x43', // Turn cursor display mode on/off
  US_E: '\x1f\x45', // Sets or cancels the blink interval of the display screen
  US_T: '\x1f\x54', // Sets the counter time and displays it in the bottom right of the screen
  US_U: '\x1f\x55', // Displays the time counter at the right side of the bottom line
  US_X: '\x1f\x58', // Sets the brightness of the fluorescent character display tube
  US_r: '\x1f\x72', // Selects or cancels reverse display of the characters received after this command
  US_v: '\x1f\x76' // Sets the DTR signal in the host interface to the MARK or SPACE state
};

/**
 * [exports description]
 * @type {[type]}
 */
var commands = _$1;

/**
 * [getParityBit description]
 * @return {[type]} [description]
 */
var getParityBit = function _getParityBit(str) {
  let parity = 0;
  const reversedCode = str.split('').reverse().join('');
  for (let counter = 0; counter < reversedCode.length; counter += 1) {
    parity += parseInt(reversedCode.charAt(counter), 10) * Math.pow(3, ((counter + 1) % 2));
  }
  return String((10 - (parity % 10)) % 10);
};

var codeLength = function _codeLength(str) {
  const buff = Buffer.from((str.length).toString(16), 'hex');
  return buff.toString();
};

var UInt8 = function _UInt8(value = 0) {
  return `0${value.toString(16)}`.slice(-2);
};

var UInt16 = function _UInt8(value = 0) {
  return `000${value.toString(16)}`.slice(-4);
};

var hexCharStr = function _hexCharStr(data = []) {
  const strArray = Array.from(data);

  return strArray
    .map(b => `\\x${`0${b.toString(16)}`.slice(-2)}`)
    .join('');
};

var utils = {
	getParityBit: getParityBit,
	codeLength: codeLength,
	UInt8: UInt8,
	UInt16: UInt16,
	hexCharStr: hexCharStr
};

/* eslint-disable */
// const iconv = require('iconv-lite');

const {
  UInt8: UInt8$1, UInt16: UInt16$1, hexCharStr: hexCharStr$1, getParityBit: getParityBit$1, codeLength: codeLength$1
} = utils;

/**
 * [function ESC/POS Encoder]
 */
function Encoder(options) {
  this.data = [];
  this.encoding = (options && options.encoding) || 'GB18030';
  this.model = null;
  this.init();
}

Encoder.create = function _create(options) {
  return new Encoder(options);
};

/**
 * Set printer model to recognize model-specific commands.
 * Supported models: [ null, 'qsprinter' ]
 *
 * For generic printers, set model to null
 *
 * [function set printer model]
 * @param  {[String]}  model [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.model = function _modelFn(_model) {
  this.model = _model;
  return this;
};

/**
 * Fix bottom margin
 * @param  {[String]} size
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.marginBottom = function _marginBottom(size) {
  this.data.push(`${commands.MARGINS.BOTTOM}\\x${UInt8$1(size)}`);
  return this;
};

/**
 * Fix left margin
 * @param  {[String]} size
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.marginLeft = function _marginLeft(size) {
  this.data.push(`${commands.MARGINS.LEFT}\\x${UInt8$1(size)}`);
  return this;
};

/**
 * Fix right margin
 * @param  {[String]} size
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.marginRight = function _marginRight(size) {
  this.data.push(`${commands.MARGINS.RIGHT}\\x${UInt8$1(size)}`);
  return this;
};

/**
 * [function print]
 * @param  {[String]}  content  [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.print = function _print(content) {
  this.data.push(content);
  return this;
};
/**
 * [function print pure content with End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.println = function _println(content) {
  return this.print(content + commands.EOL);
};

/**
 * [function print pure content with End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.newLine = function _newLine() {
  return this.print(commands.EOL);
};

/**
 * [function Print encoded alpha-numeric text with End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @param  {[String]}  encoding [optional]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.text = function _text(content, encoding) {
  // const data = iconv.encode(content + _.EOL, encoding || this.encoding);
  return this.print(content);
};

/**
 * [function Print draw line End Of Line]

 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.drawLine = function _drawLine(size = 48) {
  const content = new Array(size);
  this.print(content.fill('-').join(''));
  this.newLine();

  return this;
};

/**
 * [function Print  table   with End Of Line]
 * @param  {[List]}  data  [mandatory]
 * @param  {[String]}  encoding [optional]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.table = function _table(data, encoding) {
  const cellWidth = 48 / data.length;
  let lineTxt = '';

  for (let i = 0; i < data.length; i += 1) {
    lineTxt += data[i].toString();
    const spaces = cellWidth - data[i].toString().length;
    for (let j = 0; j < spaces; j += 1) {
      lineTxt += ' ';
    }
  }
  // this.print(iconv.encode(lineTxt + _.EOL, encoding || this.encoding));
  this.print(lineTxt + commands.EOL);
  return this;
};

/**
 * [function Print encoded alpha-numeric text without End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @param  {[String]}  encoding [optional]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.pureText = function _pureText(content, encoding) {
  // return this.print(iconv.encode(content, encoding || this.encoding));
  return this.print(content);
};

/**
 * [function encode text]
 * @param  {[String]}  encoding [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.encode = function _encode(encoding) {
  this.encoding = encoding;
  return this;
};

/**
 * [line feed]
 * @param  {[type]}    lines   [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.feed = function _feed(n = 1) {
  this.print(new Array(n).fill(commands.EOL).join(''));
  return this;
};

/**
 * [feed control sequences]
 * @param  {[type]}    ctrl     [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.control = function _control(ctrl) {
  this.print(commands.FEED_CONTROL_SEQUENCES[
    `CTL_${ctrl.toUpperCase()}`
  ]);
  return this;
};

/**
 * [text align]
 * @param  {[type]}    align    [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.align = function _align(align) {
  this.print(commands.TEXT_FORMAT[
    `TXT_ALIGN_${align.toUpperCase()}`
  ]);
  return this;
};

/**
 * [font family]
 * @param  {[type]}    family  [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.font = function _font(family) {
  this.print(commands.TEXT_FORMAT[
    `TXT_FONT_${family.toUpperCase()}`
  ]);
  return this;
};

/**
 * [font style]
 * @param  {[type]}    type     [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.style = function _style(type) {
  switch (type.toUpperCase()) {
    case 'B':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
    case 'I':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
    case 'U':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'U2':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;

    case 'BI':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
    case 'BIU':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'BIU2':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;
    case 'BU':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'BU2':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;
    case 'IU':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'IU2':
      this.print(commands.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;

    case 'NORMAL':
    default:
      this.print(commands.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(commands.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(commands.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
  }
  return this;
};

/**
 * [font size]
 * @param  {[String]}  width   [description]
 * @param  {[String]}  height  [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.size = function _size(width, height) {
  if (width <= 2 && height <= 2) {
    this.print(commands.TEXT_FORMAT.TXT_NORMAL);
    if (width === 2 && height === 2) {
      this.print(commands.TEXT_FORMAT.TXT_4SQUARE);
    } else if (width === 1 && height === 2) {
      this.print(commands.TEXT_FORMAT.TXT_2HEIGHT);
    } else if (width === 2 && height === 1) {
      this.print(commands.TEXT_FORMAT.TXT_2WIDTH);
    }
  } else {
    this.print(commands.TEXT_FORMAT.TXT_CUSTOM_SIZE(width, height));
  }
  return this;
};

/**
 * [set character spacing]
 * @param  {[type]}    n     [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.spacing = function _spacing(n) {
  if (n === undefined || n === null) {
    this.print(commands.CHARACTER_SPACING.CS_DEFAULT);
  } else {
    this.data.push(`${commands.CHARACTER_SPACING.CS_SET}\\x${UInt8$1(n)}`);
  }
  return this;
};

/**
 * [set line spacing]
 * @param  {[type]} n [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.lineSpace = function _linespace(n) {
  if (n === undefined || n === null) {
    this.print(commands.LINE_SPACING.LS_DEFAULT);
  } else {
    this.data.push(`${commands.LINE_SPACING.LS_SET}\\x${UInt8$1(n)}`);
  }
  return this;
};

Encoder.prototype.init = function _init() {
  this.hardware('INIT');
};

/**
 * [hardware]
 * @param  {[type]}    hw       [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.hardware = function _hardware(hw) {
  this.print(commands.HARDWARE[`HW_${hw.toUpperCase()}`]);
  return this;
};
/**
 * [barcode]
 * @param  {[type]}    code     [description]
 * @param  {[type]}    type     [description]
 * @param  {[type]}    options  [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */

Encoder.prototype.barcode = function _barcode(code, type = 'EAN13', options = {}) {
  let width;
  let height;
  let position;
  let font;
  let includeParity;
  if (typeof width === 'string' || typeof width === 'number') { // That's because we are not using the options.object
    width = arguments[2];
    height = arguments[3];
    position = arguments[4];
    font = arguments[5];
  } else {
    width = options.width;
    height = options.height;
    position = options.position;
    font = options.font;
    includeParity = options.includeParity !== false; // true by default
  }

  const convertCode = String(code); let parityBit = ''; let codeLength = '';
  if (typeof type === 'undefined' || type === null) {
    throw new TypeError('barcode type is required');
  }
  if (type === 'EAN13' && convertCode.length !== 12) {
    throw new Error('EAN13 Barcode type requires code length 12');
  }
  if (type === 'EAN8' && convertCode.length !== 7) {
    throw new Error('EAN8 Barcode type requires code length 7');
  }
  if (this.model === 'qsprinter') {
    this.print(commands.MODEL.QSPRINTER.BARCODE_MODE.ON);
  }
  if (this.model === 'qsprinter') ; else if (width >= 2 || width <= 6) {
    this.print(commands.BARCODE_FORMAT.BARCODE_WIDTH[width]);
  } else {
    this.print(commands.BARCODE_FORMAT.BARCODE_WIDTH_DEFAULT);
  }
  if (height >= 1 || height <= 255) {
    this.print(commands.BARCODE_FORMAT.BARCODE_HEIGHT(height));
  } else if (this.model === 'qsprinter') {
    this.print(commands.MODEL.QSPRINTER.BARCODE_HEIGHT_DEFAULT);
  } else {
    this.print(commands.BARCODE_FORMAT.BARCODE_HEIGHT_DEFAULT);
  }

  if (this.model === 'qsprinter') ; else {
    this.print(commands.BARCODE_FORMAT[
      `BARCODE_FONT_${(font || 'A').toUpperCase()}`
    ]);
  }
  this.print(commands.BARCODE_FORMAT[
    `BARCODE_TXT_${(position || 'BLW').toUpperCase()}`
  ]);
  this.print(commands.BARCODE_FORMAT[
    `BARCODE_${((type || 'EAN13').replace('-', '_').toUpperCase())}`
  ]);
  if (type === 'EAN13' || type === 'EAN8') {
    parityBit = getParityBit$1(code);
  }
  if (type === 'CODE128' || type === 'CODE93') {
    codeLength = codeLength(code);
  }
  this.print(codeLength + code + (includeParity ? parityBit : '') + '\x00'); // Allow to skip the parity byte
  if (this.model === 'qsprinter') {
    this.print(commands.MODEL.QSPRINTER.BARCODE_MODE.OFF);
  }
  return this;
};

/**
 * [print qrcode]
 * @param  {[type]} code    [description]
 * @param  {[type]} version [description]
 * @param  {[type]} level   [description]
 * @param  {[type]} size    [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.qrcode = function _qrcode(code, version = 3, level = 'L', size = 6) {
  if (this.model !== 'qsprinter') {
    this.print(commands.CODE2D_FORMAT.TYPE_QR);
    this.data.push(`${commands.CODE2D_FORMAT.CODE2D}\\x${UInt8$1(version)}`);
    this.data.push(`${commands.CODE2D_FORMAT[
      `QR_LEVEL_${(level).toUpperCase()}`
    ]}\\x${UInt8$1(size)}\\x${UInt16$1(code.length)}`);
    this.print(code);
  } else {
    // const dataRaw = iconv.encode(code, 'utf8');
    const dataRaw = code;
    if (dataRaw.length < 1 && dataRaw.length > 2710) {
      throw new Error('Invalid code length in byte. Must be between 1 and 2710');
    }

    let qrSize = size;
    // Set pixel size
    if (!size || (size && typeof size !== 'number')) {
      qrSize = commands.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.DEFAULT;
    } else if (size && size < commands.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MIN) {
      qrSize = commands.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MIN;
    } else if (size && size > commands.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MAX) {
      qrSize = commands.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MAX;
    }
    this.data.push(`${commands.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.CMD}\\x${UInt8$1(qrSize)}`);

    let qrVersion = version;
    // Set version
    if (!version || (version && typeof version !== 'number')) {
      qrVersion = commands.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.DEFAULT;
    } else if (version && version < commands.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MIN) {
      qrVersion = commands.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MIN;
    } else if (version && version > commands.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MAX) {
      qrVersion = commands.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MAX;
    }
    this.data.push(`${commands.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.CMD}\\x${UInt8$1(qrVersion)}`);

    let qrLevel = level;
    // Set level
    if (!level || (level && typeof level !== 'string')) {
      qrLevel = commands.CODE2D_FORMAT.QR_LEVEL_L;
    }
    this.print(commands.MODEL.QSPRINTER.CODE2D_FORMAT.LEVEL.CMD);
    this.print(commands.MODEL.QSPRINTER.CODE2D_FORMAT.LEVEL.OPTIONS[qrLevel.toUpperCase()]);

    // Transfer data(code) to buffer
    this.data.push(`${commands.MODEL.QSPRINTER.CODE2D_FORMAT.SAVEBUF.CMD_P1}\\x${UInt16$1(dataRaw.length + commands.MODEL.QSPRINTER.CODE2D_FORMAT.LEN_OFFSET)}`);
    this.print(commands.MODEL.QSPRINTER.CODE2D_FORMAT.SAVEBUF.CMD_P2);
    this.print(dataRaw);

    // Print from buffer
    this.data.push(`${commands.MODEL.QSPRINTER.CODE2D_FORMAT.PRINTBUF.CMD_P1}\\x${UInt16$1(dataRaw.length + commands.MODEL.QSPRINTER.CODE2D_FORMAT.LEN_OFFSET)}`);
    this.print(commands.MODEL.QSPRINTER.CODE2D_FORMAT.PRINTBUF.CMD_P2);
  }
  return this;
};

/**
 * [function Send pulse to kick the cash drawer]
 * @param  {[type]} pin [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.cashdraw = function _cashdraw(pin) {
  this.print(commands.CASH_DRAWER[
    `CD_KICK_${pin || 2}`
  ]);
  return this;
};

/**
 * Encoder Buzzer (Beep sound)
 * @param  {[Number]} n Refers to the number of buzzer times
 * @param  {[Number]} t Refers to the buzzer sound length in (t * 100) milliseconds.
 */
Encoder.prototype.beep = function _beep(n, t) {
  this.data.push(`${commands.BEEP}\\x${UInt8$1(n)}\\x${UInt8$1(t)}`);
  return this;
};

/**
 * [function Cut paper]
 * @param  {[type]} part [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.cut = function _cut(part, feed = 5) {
  this.feed(feed);
  this.print(commands.PAPER[
    part ? 'PAPER_PART_CUT' : 'PAPER_FULL_CUT'
  ]);
  return this;
};

Encoder.prototype.end = function _end() {
  const data = this.data.slice();
  this.data = [];
  this.init();
  return data;
};

/**
 * [color select between two print color modes, if your printer supports it]
 * @param  {Number} color - 0 for primary color (black) 1 for secondary color (red)
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.color = function _color(color) {
  this.print(commands.COLOR[
    color === 0 || color === 1 ? color : 0
  ]);
  return this;
};

/**
 * [reverse colors, if your printer supports it]
 * @param {Boolean} bool - True for reverse, false otherwise
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.setReverseColors = function _setReverseColors(bool) {
  this.print(bool ? commands.COLOR.REVERSE : commands.COLOR.UNREVERSE);
  return this;
};

/**
 * [exports description]
 * @type {[type]}
 */
var escpos = Encoder;

var defaultOptions = {
    keepAlive: 60,
    retries: 10
};
var PrinterProvider = /** @class */ (function (_super) {
    __extends(PrinterProvider, _super);
    function PrinterProvider(props) {
        var _this = _super.call(this, props) || this;
        _this.onCloseConnection = function () {
            _this.setState({
                isConnected: false
            });
        };
        _this.connect = function () { return __awaiter(_this, void 0, void 0, function () {
            var _a, _b, host, name, _c, enabled, options, _d;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        _a = this.props, _b = _a.host, host = _b === void 0 ? 'localhost' : _b, name = _a.name, _c = _a.enabled, enabled = _c === void 0 ? true : _c, options = _a.options;
                        if (!enabled)
                            return [2 /*return*/, {}];
                        _d = this.isActive();
                        if (!_d) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.disconnect()];
                    case 1:
                        _d = (_e.sent());
                        _e.label = 2;
                    case 2:
                        return [4 /*yield*/, qzTray.websocket.connect(__assign(__assign({ host: host }, defaultOptions), options))];
                    case 3:
                        _e.sent();
                        if (!!name) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.setDefaultPrinter()];
                    case 4:
                        _e.sent();
                        return [3 /*break*/, 6];
                    case 5:
                        this.setPrinter(name);
                        _e.label = 6;
                    case 6:
                        this.setState({ isConnected: true });
                        return [2 /*return*/, this.config];
                }
            });
        }); };
        _this.disconnect = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, qzTray.websocket.disconnect()];
        }); }); };
        _this.isActive = function () { return qzTray.websocket.isActive(); };
        _this.getConnectionInfo = function () { return qzTray.websocket.getConnectionInfo(); };
        _this.print = function (data) { return __awaiter(_this, void 0, void 0, function () {
            var isConnected, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        isConnected = this.state.isConnected;
                        _a = !isConnected;
                        if (!_a) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.connect()];
                    case 1:
                        _a = (_b.sent());
                        _b.label = 2;
                    case 2:
                        !isConnected && this.setState({ isConnected: true });
                        return [2 /*return*/, qzTray.print(this.config, data)];
                }
            });
        }); };
        _this.getPrinter = function () { return _this.config; };
        _this.listPrinters = function (query) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, qzTray.printers.find(query)];
        }); }); };
        _this.setPrinter = function (name, options) {
            _this.config = qzTray.configs.create(name, options);
            return _this.config;
        };
        _this.setDefaultPrinter = function (options) { return __awaiter(_this, void 0, void 0, function () {
            var defaultName;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, qzTray.printers.getDefault()];
                    case 1:
                        defaultName = _a.sent();
                        return [2 /*return*/, this.setPrinter(defaultName, options)];
                }
            });
        }); };
        // QZ setup
        qzTray.api.setPromiseType(function (fn) { return new Promise(fn); });
        qzTray.api.setSha256Type(function (toSign) {
            var shaObj = new JsSHA('SHA-256', 'TEXT', { encoding: 'UTF8' });
            shaObj.update(toSign);
            return shaObj.getHash('HEX');
        });
        qzRSASetup(qzTray, props.certEndpoint, props.signEndpoint);
        qzTray.websocket.setClosedCallbacks(_this.onCloseConnection);
        _this.state = {
            isConnected: false
        };
        _this.encoder = new escpos();
        return _this;
    }
    PrinterProvider.prototype.componentDidMount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.connect()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        this.setState({
                            isConnected: false
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PrinterProvider.prototype.render = function () {
        var _a = this.props, children = _a.children, _b = _a.enabled, enabled = _b === void 0 ? true : _b;
        var isConnected = this.state.isConnected;
        var qzPrinter = {
            enabled: enabled,
            print: this.print,
            listPrinters: this.listPrinters,
            getPrinter: this.getPrinter,
            setPrinter: this.setPrinter,
            setDefaultPrinter: this.setDefaultPrinter,
            isActive: this.isActive,
            connect: this.connect,
            getConnectionInfo: this.getConnectionInfo,
            disconnect: this.disconnect,
            isConnected: isConnected,
            encoder: this.encoder
        };
        return (React__default.createElement(printerContext.Provider, { value: qzPrinter }, children));
    };
    return PrinterProvider;
}(React__default.Component));

var withPrinter = function (Component) { return (function (props) { return (React__default.createElement(printerContext.Consumer, null, function (context) { return React__default.createElement(Component, __assign({}, props, { qzPrint: __assign({}, context) })); })); }); };

var index = {
    Provider: PrinterProvider,
    withPrinter: withPrinter,
    Encoder: escpos
};

exports.Encoder = escpos;
exports.Provider = PrinterProvider;
exports.default = index;
exports.withPrinter = withPrinter;
//# sourceMappingURL=index.ts.map
