export declare type Signature = {
    data: string;
    signature: string;
};
export declare function fetchCertificate(endpoint: string): Promise<string>;
export declare function signData(endpoint: string, toSign: string): Promise<Signature>;
export default function qzRSASetup(qz: any, certEndpoint: string, signEndpoint: string): void;
