import React from 'react';
declare const withPrinter: (Component: React.ElementType) => React.FunctionComponent;
export default withPrinter;
