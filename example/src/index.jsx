import { Component } from 'preact';
import Printer, { withPrinter } from 'react-qz-print';

import './style.css';
import data from './data';

function encode(encoder) {
	if (!encoder) {
		return ''
	};

	return encoder
		.font('a')
		.align('ct')
		.style('bu')
		.size(1, 1)
		.text('The quick brown fox jumps over the lazy dog')
		.newLine()
    .size(2,2)
    .align('ct')
    .setReverseColors(true)
    .text(' A123 ')
    .setReverseColors(false)
    .newLine()
		.cut(true, 10)
    .end();
}

const Print = withPrinter(({ qzPrint }) => {
	function print() {
		const d = encode(qzPrint.encoder);
		qzPrint.print(d);
	}

	return (
		<div>
			<button type="button" onClick={() => qzPrint.print(data)}>Test Print</button>
			<button type="button" onClick={() => qzPrint.connect()}>Connect</button>
			<button type="button" onClick={print}>Print Encode</button>
			<span>{qzPrint.isConnected ? ' Connected': ' Disconnected'}</span>
		</div>
	);
});

export default () => (
	<Printer.Provider
		host="smacbri"
		name="EPSON TM-P2.01"
		certEndpoint="https://dev.fast-mp.com/qz/cert"
		signEndpoint="https://dev.fast-mp.com/qz/sign"
	>
	  <div>
	    <h1>Hello, World!</h1>
			<Print />
	  </div>
	</Printer.Provider>
);
