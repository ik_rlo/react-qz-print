import { createContext } from 'react';

export type ConnectionInfo = {
  socket: string,
  host: string,
  port: number
};

export type PrinterContext = {
  enabled: boolean,
  print: (data:string) => Promise<undefined>,
  listPrinters: (query?:string) => Promise<Object>,
  getPrinter: () => Object,
  setPrinter: (name:string, options?:Object) => Object,
  setDefaultPrinter: (options?:Object) => Promise<Object>,
  isActive: () => boolean,
  getConnectionInfo: () => Promise<ConnectionInfo>,
  connect: () => Promise<Object>,
  disconnect: () => Promise<null>,
  isConnected: boolean,
  encoder: Object
};

const nullFn = () => null;
const nullPromise = () => Promise.resolve(null);

const printerContext = createContext<PrinterContext>({
  enabled: false,
  print: nullPromise,
  listPrinters: nullPromise,
  getPrinter: nullFn,
  setPrinter: nullFn,
  setDefaultPrinter: nullPromise,
  isActive: () => false,
  getConnectionInfo: nullPromise,
  connect: nullPromise,
  disconnect: nullPromise,
  isConnected: false,
  encoder: null
});

export default printerContext;
