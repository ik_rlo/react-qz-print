import Provider from './provider';
import withPrinter from './withPrinter';
import { PrinterContext } from './context';
import Encoder from './lib/escpos';

export {
  Provider,
  withPrinter,
  PrinterContext,
  Encoder
};

export default {
  Provider,
  withPrinter,
  Encoder
};
