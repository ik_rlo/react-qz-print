/* eslint-disable */
// const iconv = require('iconv-lite');
const _ = require('./commands');
const {
  UInt8, UInt16, hexCharStr, getParityBit, codeLength
} = require('./utils');

/**
 * [function ESC/POS Encoder]
 */
function Encoder(options) {
  this.data = [];
  this.encoding = (options && options.encoding) || 'GB18030';
  this.model = null;
  this.init();
}

Encoder.create = function _create(options) {
  return new Encoder(options);
}

/**
 * Set printer model to recognize model-specific commands.
 * Supported models: [ null, 'qsprinter' ]
 *
 * For generic printers, set model to null
 *
 * [function set printer model]
 * @param  {[String]}  model [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.model = function _modelFn(_model) {
  this.model = _model;
  return this;
};

/**
 * Fix bottom margin
 * @param  {[String]} size
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.marginBottom = function _marginBottom(size) {
  this.data.push(`${_.MARGINS.BOTTOM}\\x${UInt8(size)}`);
  return this;
};

/**
 * Fix left margin
 * @param  {[String]} size
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.marginLeft = function _marginLeft(size) {
  this.data.push(`${_.MARGINS.LEFT}\\x${UInt8(size)}`);
  return this;
};

/**
 * Fix right margin
 * @param  {[String]} size
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.marginRight = function _marginRight(size) {
  this.data.push(`${_.MARGINS.RIGHT}\\x${UInt8(size)}`);
  return this;
};

/**
 * [function print]
 * @param  {[String]}  content  [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.print = function _print(content) {
  this.data.push(content);
  return this;
};
/**
 * [function print pure content with End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.println = function _println(content) {
  return this.print(content + _.EOL);
};

/**
 * [function print pure content with End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.newLine = function _newLine() {
  return this.print(_.EOL);
};

/**
 * [function Print encoded alpha-numeric text with End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @param  {[String]}  encoding [optional]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.text = function _text(content, encoding) {
  // const data = iconv.encode(content + _.EOL, encoding || this.encoding);
  return this.print(content);
};

/**
 * [function Print draw line End Of Line]

 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.drawLine = function _drawLine(size = 48) {
  const content = new Array(size);
  this.print(content.fill('-').join(''));
  this.newLine();

  return this;
};

/**
 * [function Print  table   with End Of Line]
 * @param  {[List]}  data  [mandatory]
 * @param  {[String]}  encoding [optional]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.table = function _table(data, encoding) {
  const cellWidth = 48 / data.length;
  let lineTxt = '';

  for (let i = 0; i < data.length; i += 1) {
    lineTxt += data[i].toString();
    const spaces = cellWidth - data[i].toString().length;
    for (let j = 0; j < spaces; j += 1) {
      lineTxt += ' ';
    }
  }
  // this.print(iconv.encode(lineTxt + _.EOL, encoding || this.encoding));
  this.print(lineTxt + _.EOL);
  return this;
};

/**
 * [function Print encoded alpha-numeric text without End Of Line]
 * @param  {[String]}  content  [mandatory]
 * @param  {[String]}  encoding [optional]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.pureText = function _pureText(content, encoding) {
  // return this.print(iconv.encode(content, encoding || this.encoding));
  return this.print(content);
};

/**
 * [function encode text]
 * @param  {[String]}  encoding [mandatory]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.encode = function _encode(encoding) {
  this.encoding = encoding;
  return this;
};

/**
 * [line feed]
 * @param  {[type]}    lines   [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.feed = function _feed(n = 1) {
  this.print(new Array(n).fill(_.EOL).join(''));
  return this;
};

/**
 * [feed control sequences]
 * @param  {[type]}    ctrl     [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.control = function _control(ctrl) {
  this.print(_.FEED_CONTROL_SEQUENCES[
    `CTL_${ctrl.toUpperCase()}`
  ]);
  return this;
};

/**
 * [text align]
 * @param  {[type]}    align    [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.align = function _align(align) {
  this.print(_.TEXT_FORMAT[
    `TXT_ALIGN_${align.toUpperCase()}`
  ]);
  return this;
};

/**
 * [font family]
 * @param  {[type]}    family  [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.font = function _font(family) {
  this.print(_.TEXT_FORMAT[
    `TXT_FONT_${family.toUpperCase()}`
  ]);
  return this;
};

/**
 * [font style]
 * @param  {[type]}    type     [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.style = function _style(type) {
  switch (type.toUpperCase()) {
    case 'B':
      this.print(_.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
    case 'I':
      this.print(_.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
    case 'U':
      this.print(_.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'U2':
      this.print(_.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(_.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;

    case 'BI':
      this.print(_.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
    case 'BIU':
      this.print(_.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'BIU2':
      this.print(_.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(_.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;
    case 'BU':
      this.print(_.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'BU2':
      this.print(_.TEXT_FORMAT.TXT_BOLD_ON);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(_.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;
    case 'IU':
      this.print(_.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_ON);
      break;
    case 'IU2':
      this.print(_.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_ON);
      this.print(_.TEXT_FORMAT.TXT_UNDERL2_ON);
      break;

    case 'NORMAL':
    default:
      this.print(_.TEXT_FORMAT.TXT_BOLD_OFF);
      this.print(_.TEXT_FORMAT.TXT_ITALIC_OFF);
      this.print(_.TEXT_FORMAT.TXT_UNDERL_OFF);
      break;
  }
  return this;
};

/**
 * [font size]
 * @param  {[String]}  width   [description]
 * @param  {[String]}  height  [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.size = function _size(width, height) {
  if (width <= 2 && height <= 2) {
    this.print(_.TEXT_FORMAT.TXT_NORMAL);
    if (width === 2 && height === 2) {
      this.print(_.TEXT_FORMAT.TXT_4SQUARE);
    } else if (width === 1 && height === 2) {
      this.print(_.TEXT_FORMAT.TXT_2HEIGHT);
    } else if (width === 2 && height === 1) {
      this.print(_.TEXT_FORMAT.TXT_2WIDTH);
    }
  } else {
    this.print(_.TEXT_FORMAT.TXT_CUSTOM_SIZE(width, height));
  }
  return this;
};

/**
 * [set character spacing]
 * @param  {[type]}    n     [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.spacing = function _spacing(n) {
  if (n === undefined || n === null) {
    this.print(_.CHARACTER_SPACING.CS_DEFAULT);
  } else {
    this.data.push(`${_.CHARACTER_SPACING.CS_SET}\\x${UInt8(n)}`);
  }
  return this;
};

/**
 * [set line spacing]
 * @param  {[type]} n [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.lineSpace = function _linespace(n) {
  if (n === undefined || n === null) {
    this.print(_.LINE_SPACING.LS_DEFAULT);
  } else {
    this.data.push(`${_.LINE_SPACING.LS_SET}\\x${UInt8(n)}`);
  }
  return this;
};

Encoder.prototype.init = function _init() {
  this.hardware('INIT');
};

/**
 * [hardware]
 * @param  {[type]}    hw       [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.hardware = function _hardware(hw) {
  this.print(_.HARDWARE[`HW_${hw.toUpperCase()}`]);
  return this;
};
/**
 * [barcode]
 * @param  {[type]}    code     [description]
 * @param  {[type]}    type     [description]
 * @param  {[type]}    options  [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */

Encoder.prototype.barcode = function _barcode(code, type = 'EAN13', options = {}) {
  let width;
  let height;
  let position;
  let font;
  let includeParity;
  if (typeof width === 'string' || typeof width === 'number') { // That's because we are not using the options.object
    width = arguments[2];
    height = arguments[3];
    position = arguments[4];
    font = arguments[5];
  } else {
    width = options.width;
    height = options.height;
    position = options.position;
    font = options.font;
    includeParity = options.includeParity !== false; // true by default
  }

  const convertCode = String(code); let parityBit = ''; let codeLength = '';
  if (typeof type === 'undefined' || type === null) {
    throw new TypeError('barcode type is required');
  }
  if (type === 'EAN13' && convertCode.length !== 12) {
    throw new Error('EAN13 Barcode type requires code length 12');
  }
  if (type === 'EAN8' && convertCode.length !== 7) {
    throw new Error('EAN8 Barcode type requires code length 7');
  }
  if (this.model === 'qsprinter') {
    this.print(_.MODEL.QSPRINTER.BARCODE_MODE.ON);
  }
  if (this.model === 'qsprinter') {
    // qsprinter has no BARCODE_WIDTH command (as of v7.5)
  } else if (width >= 2 || width <= 6) {
    this.print(_.BARCODE_FORMAT.BARCODE_WIDTH[width]);
  } else {
    this.print(_.BARCODE_FORMAT.BARCODE_WIDTH_DEFAULT);
  }
  if (height >= 1 || height <= 255) {
    this.print(_.BARCODE_FORMAT.BARCODE_HEIGHT(height));
  } else if (this.model === 'qsprinter') {
    this.print(_.MODEL.QSPRINTER.BARCODE_HEIGHT_DEFAULT);
  } else {
    this.print(_.BARCODE_FORMAT.BARCODE_HEIGHT_DEFAULT);
  }

  if (this.model === 'qsprinter') {
    // Qsprinter has no barcode font
  } else {
    this.print(_.BARCODE_FORMAT[
      `BARCODE_FONT_${(font || 'A').toUpperCase()}`
    ]);
  }
  this.print(_.BARCODE_FORMAT[
    `BARCODE_TXT_${(position || 'BLW').toUpperCase()}`
  ]);
  this.print(_.BARCODE_FORMAT[
    `BARCODE_${((type || 'EAN13').replace('-', '_').toUpperCase())}`
  ]);
  if (type === 'EAN13' || type === 'EAN8') {
    parityBit = getParityBit(code);
  }
  if (type === 'CODE128' || type === 'CODE93') {
    codeLength = codeLength(code);
  }
  this.print(codeLength + code + (includeParity ? parityBit : '') + '\x00'); // Allow to skip the parity byte
  if (this.model === 'qsprinter') {
    this.print(_.MODEL.QSPRINTER.BARCODE_MODE.OFF);
  }
  return this;
};

/**
 * [print qrcode]
 * @param  {[type]} code    [description]
 * @param  {[type]} version [description]
 * @param  {[type]} level   [description]
 * @param  {[type]} size    [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.qrcode = function _qrcode(code, version = 3, level = 'L', size = 6) {
  if (this.model !== 'qsprinter') {
    this.print(_.CODE2D_FORMAT.TYPE_QR);
    this.data.push(`${_.CODE2D_FORMAT.CODE2D}\\x${UInt8(version)}`);
    this.data.push(`${_.CODE2D_FORMAT[
      `QR_LEVEL_${(level).toUpperCase()}`
    ]}\\x${UInt8(size)}\\x${UInt16(code.length)}`);
    this.print(code);
  } else {
    // const dataRaw = iconv.encode(code, 'utf8');
    const dataRaw = code;
    if (dataRaw.length < 1 && dataRaw.length > 2710) {
      throw new Error('Invalid code length in byte. Must be between 1 and 2710');
    }

    let qrSize = size;
    // Set pixel size
    if (!size || (size && typeof size !== 'number')) {
      qrSize = _.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.DEFAULT;
    } else if (size && size < _.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MIN) {
      qrSize = _.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MIN;
    } else if (size && size > _.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MAX) {
      qrSize = _.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.MAX;
    }
    this.data.push(`${_.MODEL.QSPRINTER.CODE2D_FORMAT.PIXEL_SIZE.CMD}\\x${UInt8(qrSize)}`);

    let qrVersion = version;
    // Set version
    if (!version || (version && typeof version !== 'number')) {
      qrVersion = _.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.DEFAULT;
    } else if (version && version < _.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MIN) {
      qrVersion = _.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MIN;
    } else if (version && version > _.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MAX) {
      qrVersion = _.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.MAX;
    }
    this.data.push(`${_.MODEL.QSPRINTER.CODE2D_FORMAT.VERSION.CMD}\\x${UInt8(qrVersion)}`);

    let qrLevel = level;
    // Set level
    if (!level || (level && typeof level !== 'string')) {
      qrLevel = _.CODE2D_FORMAT.QR_LEVEL_L;
    }
    this.print(_.MODEL.QSPRINTER.CODE2D_FORMAT.LEVEL.CMD);
    this.print(_.MODEL.QSPRINTER.CODE2D_FORMAT.LEVEL.OPTIONS[qrLevel.toUpperCase()]);

    // Transfer data(code) to buffer
    this.data.push(`${_.MODEL.QSPRINTER.CODE2D_FORMAT.SAVEBUF.CMD_P1}\\x${UInt16(dataRaw.length + _.MODEL.QSPRINTER.CODE2D_FORMAT.LEN_OFFSET)}`);
    this.print(_.MODEL.QSPRINTER.CODE2D_FORMAT.SAVEBUF.CMD_P2);
    this.print(dataRaw);

    // Print from buffer
    this.data.push(`${_.MODEL.QSPRINTER.CODE2D_FORMAT.PRINTBUF.CMD_P1}\\x${UInt16(dataRaw.length + _.MODEL.QSPRINTER.CODE2D_FORMAT.LEN_OFFSET)}`);
    this.print(_.MODEL.QSPRINTER.CODE2D_FORMAT.PRINTBUF.CMD_P2);
  }
  return this;
};

/**
 * [function Send pulse to kick the cash drawer]
 * @param  {[type]} pin [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.cashdraw = function _cashdraw(pin) {
  this.print(_.CASH_DRAWER[
    `CD_KICK_${pin || 2}`
  ]);
  return this;
};

/**
 * Encoder Buzzer (Beep sound)
 * @param  {[Number]} n Refers to the number of buzzer times
 * @param  {[Number]} t Refers to the buzzer sound length in (t * 100) milliseconds.
 */
Encoder.prototype.beep = function _beep(n, t) {
  this.data.push(`${_.BEEP}\\x${UInt8(n)}\\x${UInt8(t)}`);
  return this;
};

/**
 * [function Cut paper]
 * @param  {[type]} part [description]
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.cut = function _cut(part, feed = 5) {
  this.feed(feed);
  this.print(_.PAPER[
    part ? 'PAPER_PART_CUT' : 'PAPER_FULL_CUT'
  ]);
  return this;
};

Encoder.prototype.end = function _end() {
  const data = this.data.slice();
  this.data = [];
  this.init();
  return data;
};

/**
 * [color select between two print color modes, if your printer supports it]
 * @param  {Number} color - 0 for primary color (black) 1 for secondary color (red)
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.color = function _color(color) {
  this.print(_.COLOR[
    color === 0 || color === 1 ? color : 0
  ]);
  return this;
};

/**
 * [reverse colors, if your printer supports it]
 * @param {Boolean} bool - True for reverse, false otherwise
 * @return {[Encoder]} printer  [the escpos printer instance]
 */
Encoder.prototype.setReverseColors = function _setReverseColors(bool) {
  this.print(bool ? _.COLOR.REVERSE : _.COLOR.UNREVERSE);
  return this;
};

/**
 * [exports description]
 * @type {[type]}
 */
module.exports = Encoder;
