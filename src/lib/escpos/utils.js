/**
 * [getParityBit description]
 * @return {[type]} [description]
 */
exports.getParityBit = function _getParityBit(str) {
  let parity = 0;
  const reversedCode = str.split('').reverse().join('');
  for (let counter = 0; counter < reversedCode.length; counter += 1) {
    parity += parseInt(reversedCode.charAt(counter), 10) * Math.pow(3, ((counter + 1) % 2));
  }
  return String((10 - (parity % 10)) % 10);
};

exports.codeLength = function _codeLength(str) {
  const buff = Buffer.from((str.length).toString(16), 'hex');
  return buff.toString();
};

exports.UInt8 = function _UInt8(value = 0) {
  return `0${value.toString(16)}`.slice(-2);
};

exports.UInt16 = function _UInt8(value = 0) {
  return `000${value.toString(16)}`.slice(-4);
};

exports.hexCharStr = function _hexCharStr(data = []) {
  const strArray = Array.from(data);

  return strArray
    .map(b => `\\x${`0${b.toString(16)}`.slice(-2)}`)
    .join('');
};
