/* eslint-env browser */

type ResolveFn = (value?: unknown) => void;
type RejectFn = (reason?: unknown) => void;

export type Signature = {
  data: string,
  signature: string
};

export async function fetchCertificate(endpoint:string):Promise<string> {
  if (endpoint) {
    const res = await fetch(endpoint);
    return res.text();
  }
  return '';
}

export async function signData(endpoint:string, toSign:string):Promise<Signature> {
  if (endpoint) {
    const res = await fetch(endpoint, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: JSON.stringify({ toSign })
    });
    return res.json();
  }

  return {
    data: toSign,
    signature: ''
  };
}

export default function qzRSASetup(qz:any, certEndpoint:string, signEndpoint:string) {
  qz.security.setCertificatePromise((resolve:ResolveFn, reject:RejectFn) => {
    fetchCertificate(certEndpoint)
      .then(cert => resolve(cert))
      .catch(reject);
  });
  qz.security.setSignatureAlgorithm('SHA512');
  qz.security.setSignaturePromise((toSign:string) => (resolve:ResolveFn, reject:RejectFn) => {
    signData(signEndpoint, toSign)
      .then(sign => resolve(sign.signature))
      .catch(reject);
  });
}
