import React from 'react';
import qz from 'qz-tray';
import JsSHA from 'jssha';

import Context from './context';
import qzRSASetup from './lib/qz-rsa';
import Encoder from './lib/escpos';

type ResolveFn = (value?: unknown) => void;
type RejectFn = (reason?: unknown) => void;
type PromiseFn = (resolve: ResolveFn, reject: RejectFn) => void;

type ConnectionOptions = {
  port?: {
    secure?: Number[],
    insecure?: Number[]
  },
  usingSecure?: boolean,
  keepAlive?: number,
  retries?: number,
  delay?: number
}

type Props = {
  enabled?: boolean
  host?: string,
  name?: string,
  certEndpoint: string,
  signEndpoint: string,
  options?: ConnectionOptions
};

type State = {
  isConnected: boolean,
};

const defaultOptions:ConnectionOptions = {
  keepAlive: 60,
  retries: 10
};

class PrinterProvider extends React.Component<Props, State> {
  config: Object

  encoder: Object

  constructor(props:Props) {
    super(props);

    // QZ setup
    qz.api.setPromiseType((fn:PromiseFn) => new Promise(fn));
    qz.api.setSha256Type((toSign:string) => {
      const shaObj = new JsSHA('SHA-256', 'TEXT', { encoding: 'UTF8' });
      shaObj.update(toSign);
      return shaObj.getHash('HEX');
    });
    qzRSASetup(qz, props.certEndpoint, props.signEndpoint);

    qz.websocket.setClosedCallbacks(this.onCloseConnection);

    this.state = {
      isConnected: false
    };
    this.encoder = new Encoder();
  }

  async componentDidMount() {
    try {
      await this.connect();
    } catch (error) {
      this.setState({
        isConnected: false
      });
    }
  }

  onCloseConnection = () => {
    this.setState({
      isConnected: false
    });
  }

  connect = async () => {
    const {
      host = 'localhost', name, enabled = true, options
    } = this.props;

    if (!enabled) return {};

    this.isActive() && await this.disconnect();
    await qz.websocket.connect({ host, ...defaultOptions, ...options });
    if (!name) {
      await this.setDefaultPrinter();
    } else {
      this.setPrinter(name);
    }

    this.setState({ isConnected: true });
    return this.config;
  }

  disconnect = async () => qz.websocket.disconnect();

  isActive = () => qz.websocket.isActive();

  getConnectionInfo = () => qz.websocket.getConnectionInfo();

  print = async (data:string) => {
    const { isConnected } = this.state;

    !isConnected && await this.connect();
    !isConnected && this.setState({ isConnected: true });
    return qz.print(this.config, data);
  };

  getPrinter = () => this.config;

  listPrinters = async (query:string) => qz.printers.find(query);

  setPrinter = (name:string, options?:Object) => {
    this.config = qz.configs.create(name, options);
    return this.config;
  };

  setDefaultPrinter = async (options?:Object) => {
    const defaultName = await qz.printers.getDefault();
    return this.setPrinter(defaultName, options);
  };

  render() {
    const { children, enabled = true } = this.props;
    const { isConnected } = this.state;
    const qzPrinter = {
      enabled,
      print: this.print,
      listPrinters: this.listPrinters,
      getPrinter: this.getPrinter,
      setPrinter: this.setPrinter,
      setDefaultPrinter: this.setDefaultPrinter,
      isActive: this.isActive,
      connect: this.connect,
      getConnectionInfo: this.getConnectionInfo,
      disconnect: this.disconnect,
      isConnected,
      encoder: this.encoder
    };

    return (
      <Context.Provider value={qzPrinter}>
        {children}
      </Context.Provider>
    );
  }
}

export default PrinterProvider;
