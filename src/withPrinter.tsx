import React from 'react';
import Context from './context';

const withPrinter = (Component:React.ElementType):React.FunctionComponent => (
  props => (
    <Context.Consumer>
      {context => <Component {...props} qzPrint={{ ...context }} />}
    </Context.Consumer>
  )
);

export default withPrinter;
